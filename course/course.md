# 20220703 - Web Avancé - Rennes -  Fullstack demo3

Utiliser jHipster pour configurer un environnement de développement full stack monolithique avec Angular et Spring Boot.

Quick start
https://www.jhipster.tech/

jHipster ne sépare pas les parties client & serveur dans 2 sous projets distincts.
Les parties de code concernées sont placées dans 2 sous dossiers de ./src/main :
- Spring Boot dans ./src/main/java
- Angular dans ./src/main/webapp 


## Créer un premier projet full stack

```bash

### Ne pas travailler sur un DD externe, 'npm install' plante avec une erreur du type :
#     npm ERR! Error: EPERM: operation not permitted, symlink '../@angular/cli/bin/ng.js' 
#           -> '/media/pierre/LaCie/projets/campus-academy/fullstack4pro/fullstack-m1&2-rennes/exo/fullstack/tutorial-jhipster-docker-demo3/node_modules/.bin/ng'
# cd /media/pierre/LaCie/projets/campus-academy/fullstack4pro/"fullstack-m1&2-rennes"/exo/fullstack
cd ~/workspace/tutorial-jhipster-docker-demo3

#################################################################################################
###
### Vérifier la configuration de travail sous Linux (Ubuntu)
###
#################################################################################################


java -version
# openjdk version "11.0.15" 2022-04-19
# OpenJDK Runtime Environment (build 11.0.15+10-Ubuntu-0ubuntu0.22.04.1)
# OpenJDK 64-Bit Server VM (build 11.0.15+10-Ubuntu-0ubuntu0.22.04.1, mixed mode, sharing)

node -v
# v14.19.3

npm -v
# 6.14.17

git --version
# git version 2.34.1

### Si besoin installé jHipster
npm install -g generator-jhipster
# npm WARN deprecated querystring@0.2.0: The querystring API is considered Legacy. new code should use the URLSearchParams API instead.
# npm WARN deprecated uuid@3.3.2: Please upgrade  to version 7 or higher.  Older versions may use Math.random() in certain circumstances, which is known to be problematic.  See https://v8.dev/blog/math-random for details.
# npm WARN deprecated request@2.88.2: request has been deprecated, see https://github.com/request/request/issues/3142
# npm WARN deprecated har-validator@5.1.5: this library is no longer supported
# npm WARN deprecated uuid@3.4.0: Please upgrade  to version 7 or higher.  Older versions may use Math.random() in certain circumstances, which is known to be problematic.  See https://v8.dev/blog/math-random for details.
# /home/pierre/.nvm/versions/node/v14.19.3/bin/jhipster -> /home/pierre/.nvm/versions/node/v14.19.3/lib/node_modules/generator-jhipster/cli/jhipster.js
# + generator-jhipster@7.8.1
# added 603 packages from 437 contributors in 67.797s
# 

jhipster --version
# INFO! Using bundled JHipster
# 7.8.1

### accepter les relances de service proposées par défaut
sudo apt  install docker.io -y
# Reading package lists... Done
# Building dependency tree... Done
# Reading state information... Done
# The following package was automatically installed and is no longer required:
#   libqt5concurrent5
# Use 'sudo apt autoremove' to remove it.
# The following additional packages will be installed:
#   bridge-utils containerd pigz runc ubuntu-fan
# Suggested packages:
#   ifupdown aufs-tools cgroupfs-mount | cgroup-lite debootstrap docker-doc rinse zfs-fuse | zfsutils
# The following NEW packages will be installed:
#   bridge-utils containerd docker.io pigz runc ubuntu-fan
# 0 upgraded, 6 newly installed, 0 to remove and 99 not upgraded.
# Need to get 65.3 MB of archives.
# After this operation, 282 MB of additional disk space will be used.
# Get:1 http://fr.archive.ubuntu.com/ubuntu jammy/universe amd64 pigz amd64 2.6-1 [63.6 kB]
# Get:2 http://fr.archive.ubuntu.com/ubuntu jammy/main amd64 bridge-utils amd64 1.7-1ubuntu3 [34.4 kB]
# Get:3 http://fr.archive.ubuntu.com/ubuntu jammy/main amd64 runc amd64 1.1.0-0ubuntu1 [4,087 kB]
# Get:4 http://fr.archive.ubuntu.com/ubuntu jammy/main amd64 containerd amd64 1.5.9-0ubuntu3 [27.0 MB]
# Get:5 http://fr.archive.ubuntu.com/ubuntu jammy/universe amd64 docker.io amd64 20.10.12-0ubuntu4 [34.0 MB]
# Get:6 http://fr.archive.ubuntu.com/ubuntu jammy/universe amd64 ubuntu-fan all 0.12.16 [35.2 kB]                                                                                                                                                                                  
# Fetched 65.3 MB in 9s (7,048 kB/s)                                                                                                                                                                                                                                               
# Preconfiguring packages ...
# Selecting previously unselected package pigz.
# (Reading database ... 259906 files and directories currently installed.)
# Preparing to unpack .../0-pigz_2.6-1_amd64.deb ...
# Unpacking pigz (2.6-1) ...
# Selecting previously unselected package bridge-utils.
# Preparing to unpack .../1-bridge-utils_1.7-1ubuntu3_amd64.deb ...
# Unpacking bridge-utils (1.7-1ubuntu3) ...
# Selecting previously unselected package runc.
# Preparing to unpack .../2-runc_1.1.0-0ubuntu1_amd64.deb ...
# Unpacking runc (1.1.0-0ubuntu1) ...
# Selecting previously unselected package containerd.
# Preparing to unpack .../3-containerd_1.5.9-0ubuntu3_amd64.deb ...
# Unpacking containerd (1.5.9-0ubuntu3) ...
# Selecting previously unselected package docker.io.
# Preparing to unpack .../4-docker.io_20.10.12-0ubuntu4_amd64.deb ...
# Unpacking docker.io (20.10.12-0ubuntu4) ...
# Selecting previously unselected package ubuntu-fan.
# Preparing to unpack .../5-ubuntu-fan_0.12.16_all.deb ...
# Unpacking ubuntu-fan (0.12.16) ...
# Setting up runc (1.1.0-0ubuntu1) ...
# Setting up bridge-utils (1.7-1ubuntu3) ...
# Setting up pigz (2.6-1) ...
# Setting up containerd (1.5.9-0ubuntu3) ...
# Created symlink /etc/systemd/system/multi-user.target.wants/containerd.service → /lib/systemd/system/containerd.service.
# Setting up ubuntu-fan (0.12.16) ...
# Created symlink /etc/systemd/system/multi-user.target.wants/ubuntu-fan.service → /lib/systemd/system/ubuntu-fan.service.
# Setting up docker.io (20.10.12-0ubuntu4) ...
# Adding group `docker' (GID 137) ...
# Done.
# Created symlink /etc/systemd/system/multi-user.target.wants/docker.service → /lib/systemd/system/docker.service.
# Created symlink /etc/systemd/system/sockets.target.wants/docker.socket → /lib/systemd/system/docker.socket.
# Processing triggers for man-db (2.10.2-1) ...
# Scanning processes...                                              
# Scanning candidates...                                             
# Scanning processor microcode...                                                       
# Scanning linux images...                                                                                       
# 
# Running kernel seems to be up-to-date.
# 
# The processor microcode seems to be up-to-date.
# 
# Restarting services...
#  /etc/needrestart/restart.d/systemd-manager
#  systemctl restart packagekit.service ssh.service systemd-journald.service systemd-networkd.service systemd-oomd.service systemd-resolved.service systemd-timesyncd.service systemd-udevd.service udisks2.service upower.service
# Service restarts being deferred:
#  systemctl restart NetworkManager.service
#  systemctl restart lightdm.service
#  systemctl restart systemd-logind.service
#  systemctl restart user@1000.service
#  systemctl restart wpa_supplicant.service
# 
# No containers need to be restarted.
# 
# No user sessions are running outdated binaries.
# 
# No VM guests are running outdated hypervisor (qemu) binaries on this host.

docker -v
# Docker version 20.10.12, build 20.10.12-0ubuntu4

### accepter les relances de service proposées par défaut
sudo apt  install docker-compose -y
# Reading package lists... Done
# Building dependency tree... Done
# Reading state information... Done
# The following package was automatically installed and is no longer required:
#   libqt5concurrent5
# Use 'sudo apt autoremove' to remove it.
# The following additional packages will be installed:
#   python3-docker python3-dockerpty python3-docopt python3-dotenv python3-websocket
# The following NEW packages will be installed:
#   docker-compose python3-docker python3-dockerpty python3-docopt python3-dotenv python3-websocket
# 0 upgraded, 6 newly installed, 0 to remove and 99 not upgraded.
# Need to get 278 kB of archives.
# After this operation, 1,492 kB of additional disk space will be used.
# Get:1 http://fr.archive.ubuntu.com/ubuntu jammy/universe amd64 python3-websocket all 1.2.3-1 [34.7 kB]
# Get:2 http://fr.archive.ubuntu.com/ubuntu jammy/universe amd64 python3-docker all 5.0.3-1 [89.3 kB]
# Get:3 http://fr.archive.ubuntu.com/ubuntu jammy/universe amd64 python3-dockerpty all 0.4.1-2 [11.1 kB]
# Get:4 http://fr.archive.ubuntu.com/ubuntu jammy/universe amd64 python3-docopt all 0.6.2-4 [26.9 kB]
# Get:5 http://fr.archive.ubuntu.com/ubuntu jammy/universe amd64 python3-dotenv all 0.19.2-1 [20.5 kB]
# Get:6 http://fr.archive.ubuntu.com/ubuntu jammy/universe amd64 docker-compose all 1.29.2-1 [95.8 kB]
# Fetched 278 kB in 0s (987 kB/s)          
# Selecting previously unselected package python3-websocket.
# (Reading database ... 260231 files and directories currently installed.)
# Preparing to unpack .../0-python3-websocket_1.2.3-1_all.deb ...
# Unpacking python3-websocket (1.2.3-1) ...
# Selecting previously unselected package python3-docker.
# Preparing to unpack .../1-python3-docker_5.0.3-1_all.deb ...
# Unpacking python3-docker (5.0.3-1) ...
# Selecting previously unselected package python3-dockerpty.
# Preparing to unpack .../2-python3-dockerpty_0.4.1-2_all.deb ...
# Unpacking python3-dockerpty (0.4.1-2) ...
# Selecting previously unselected package python3-docopt.
# Preparing to unpack .../3-python3-docopt_0.6.2-4_all.deb ...
# Unpacking python3-docopt (0.6.2-4) ...
# Selecting previously unselected package python3-dotenv.
# Preparing to unpack .../4-python3-dotenv_0.19.2-1_all.deb ...
# Unpacking python3-dotenv (0.19.2-1) ...
# Selecting previously unselected package docker-compose.
# Preparing to unpack .../5-docker-compose_1.29.2-1_all.deb ...
# Unpacking docker-compose (1.29.2-1) ...
# Setting up python3-dotenv (0.19.2-1) ...
# Setting up python3-docopt (0.6.2-4) ...
# Setting up python3-websocket (1.2.3-1) ...
# Setting up python3-dockerpty (0.4.1-2) ...
# Setting up python3-docker (5.0.3-1) ...
# Setting up docker-compose (1.29.2-1) ...
# Processing triggers for man-db (2.10.2-1) ...
# Scanning processes...                                              
# Scanning candidates...                                             
# Scanning processor microcode...                                                                  
# Scanning linux images...                                                                                                  
# 
# Running kernel seems to be up-to-date.
# 
# The processor microcode seems to be up-to-date.
# 
# Restarting services...
# Service restarts being deferred:
#  systemctl restart NetworkManager.service
#  systemctl restart lightdm.service
#  systemctl restart systemd-logind.service
#  systemctl restart user@1000.service
#  systemctl restart wpa_supplicant.service
# 
# No containers need to be restarted.
# 
# No user sessions are running outdated binaries.
# 
# No VM guests are running outdated hypervisor (qemu) binaries on this host.


#################################################################################################
###
### Créer un projet
###
#################################################################################################
#  jHipster procède en 3 temps :
#  - génération des fichiers sources : jHipster crée les fichiers sources pour Spring Boot & Angular, les fichiers de configuration et les fichiers de construction pour Maven & npm.
#  - installation des modules npm : un projet Angular utilise de nombreuses bibliothèques npm. npm les charge (cf. 'npm install'), ce qui prend un certain temps 
#  - création du dépôt Git : cette étape finale initialise un dépôt Git local


mkdir -pv tutorial-jhipster-docker-demo3 && cd $_
# mkdir: created directory 'tutorial-jhipster-docker-demo3'

jhipster
# INFO! Using bundled JHipster
# 
#         ██╗ ██╗   ██╗ ████████╗ ███████╗   ██████╗ ████████╗ ████████╗ ███████╗
#         ██║ ██║   ██║ ╚══██╔══╝ ██╔═══██╗ ██╔════╝ ╚══██╔══╝ ██╔═════╝ ██╔═══██╗
#         ██║ ████████║    ██║    ███████╔╝ ╚█████╗     ██║    ██████╗   ███████╔╝
#   ██╗   ██║ ██╔═══██║    ██║    ██╔════╝   ╚═══██╗    ██║    ██╔═══╝   ██╔══██║
#   ╚██████╔╝ ██║   ██║ ████████╗ ██║       ██████╔╝    ██║    ████████╗ ██║  ╚██╗
#    ╚═════╝  ╚═╝   ╚═╝ ╚═══════╝ ╚═╝       ╚═════╝     ╚═╝    ╚═══════╝ ╚═╝   ╚═╝
#                             https://www.jhipster.tech
# Welcome to JHipster v7.8.1
# 
# Application files will be generated in folder: /media/pierre/LaCie/projets/campus-academy/fullstack4pro/fullstack-m1&2-rennes/exo/fullstack/tutorial-jhipster-docker-demo3
#  _______________________________________________________________________________________________________________
# 
#   Documentation for creating an application is at https://www.jhipster.tech/creating-an-app/
#   If you find JHipster useful, consider sponsoring the project at https://opencollective.com/generator-jhipster
#  _______________________________________________________________________________________________________________
# 
# ? Which *type* of application would you like to create? Monolithic application (recommended for simple projects)
#   Monolithic application (recommended for simple projects) 
#   Gateway application 
#   Microservice application 
# ? What is the base name of your application? (jhipster) demo3
# ? Do you want to make it reactive with Spring WebFlux? (y/N) No
# ? What is your default Java package name? (com.mycompany.myapp) academy.campus.demo3
# ? Which *type* of authentication would you like to use? JWT authentication (stateless, with a token)
#   JWT authentication (stateless, with a token) 
#   OAuth 2.0 / OIDC Authentication (stateful, works with Keycloak and Okta) 
#   HTTP Session Authentication (stateful, default Spring Security mechanism) 
# ? Which *type* of database would you like to use? SQL (H2, PostgreSQL, MySQL, MariaDB, Oracle, MSSQL)
#   SQL (H2, PostgreSQL, MySQL, MariaDB, Oracle, MSSQL) 
#   MongoDB 
#   Cassandra 
#   [BETA] Couchbase 
#   [BETA] Neo4j 
#   No database 
# ? Which *production* database would you like to use? PostgreSQL
#   PostgreSQL 
#   MySQL 
#   MariaDB 
#   Oracle 
#   Microsoft SQL Server 
# ? Which *development* database would you like to use? H2 with in-memory persistence
#   H2 with disk-based persistence 
#   H2 with in-memory persistence 
#   PostgreSQL 
# ? Which cache do you want to use? (Spring cache abstraction) Ehcache (local cache, for a single node)
#   Ehcache (local cache, for a single node) 
#   Caffeine (local cache, for a single node) 
#   Hazelcast (distributed cache, for multiple nodes, supports rate-limiting for gateway applications) 
#   Infinispan (hybrid cache, for multiple nodes) 
#   Memcached (distributed cache) - Warning, when using an SQL database, this will disable the Hibernate 2nd level cache! 
#   Redis (distributed cache) 
#   No cache - Warning, when using an SQL database, this will disable the Hibernate 2nd level cache! 
# ? Do you want to use Hibernate 2nd level cache? (Y/n) Yes
# ? Would you like to use Maven or Gradle for building the backend? Maven
#   Maven 
#   Gradle 
# ? Do you want to use the JHipster Registry to configure, monitor and scale your application? No
#   No
#   Yes
# ? Which other technologies would you like to use? None
#  ◯ Elasticsearch as search engine
#  ◯ WebSockets using Spring Websocket
#  ◯ Apache Kafka as asynchronous messages broker
#  ◯ API first development using OpenAPI-generator
# ? Which *Framework* would you like to use for the client? Angular
#   Angular 
#   React 
#   Vue 
#   No client 
# ? Do you want to generate the admin UI? (Y/n) Yes
# ? Would you like to use a Bootswatch theme (https://bootswatch.com/)? Default JHipster
#   Default JHipster 
#   Cerulean 
#   Cosmo 
#   Cyborg 
#   Darkly 
#   Flatly 
#   Journal 
# ? Would you like to enable internationalization support? (Y/n) Yes
# ? Please choose the native language of the application English
#   English 
#   Estonian 
#   Farsi 
#   Finnish 
#   French 
#   Galician 
#   German 
# ? Please choose additional languages to install French
#  ◯ Estonian
#  ◯ Farsi
#  ◯ Finnish
# ❯◉ French
#  ◯ Galician
#  ◯ German
#  ◯ Greek
# ? Besides JUnit and Jest, which testing frameworks would you like to use? None
#  ◯ Cypress
#  ◯ [DEPRECATED] Protractor
#  ◯ Gatling
#  ◯ Cucumber
# ? Would you like to install other generators from the JHipster Marketplace? (y/N) No
# 
# KeyStore '/home/pierre/workspace/tutorial-jhipster-docker-demo3/src/main/resources/config/tls//keystore.p12' generated successfully.
# 
#    create .prettierrc
#    create .prettierignore
#    create package.json
#     force .yo-rc-global.json
#     force .yo-rc.json
#    create .gitattributes
#    create .editorconfig
#    create .gitignore
#    create sonar-project.properties
#    create .lintstagedrc.js
#    create .husky/pre-commit
#    create mvnw
#    create mvnw.cmd
#    create .mvn/jvm.config
#    create .mvn/wrapper/maven-wrapper.jar
#    create .mvn/wrapper/maven-wrapper.properties
#    create npmw
#    create npmw.cmd
#    create src/main/resources/banner.txt
#    create src/main/resources/config/liquibase/changelog/00000000000000_initial_schema.xml
#    create src/main/resources/config/liquibase/master.xml
#    create src/main/docker/jib/entrypoint.sh
#    create checkstyle.xml
#    create .devcontainer/Dockerfile
#    create pom.xml
#    create src/main/resources/logback-spring.xml
#    create src/main/resources/i18n/messages.properties
#    create src/main/resources/.h2.server.properties
#    create .devcontainer/devcontainer.json
#    create src/main/docker/app.yml
#    create src/main/docker/jhipster-control-center.yml
#    create src/main/docker/postgresql.yml
#    create src/main/docker/central-server-config/README.md
#    create src/main/docker/sonar.yml
#    create src/main/docker/zipkin.yml
#    create src/main/docker/monitoring.yml
#    create src/main/resources/templates/error.html
#    create src/main/docker/prometheus/prometheus.yml
#    create src/main/docker/grafana/provisioning/dashboards/dashboard.yml
#    create src/main/docker/grafana/provisioning/dashboards/JVM.json
#    create src/main/resources/config/application.yml
#    create src/main/resources/config/application-dev.yml
#    create src/main/docker/grafana/provisioning/datasources/datasource.yml
#    create src/main/resources/config/application-tls.yml
#    create src/main/resources/config/application-prod.yml
#    create src/main/resources/config/bootstrap.yml
#    create src/main/resources/config/bootstrap-prod.yml
#    create src/main/java/academy/campus/demo3/security/SpringSecurityAuditorAware.java
#    create src/main/java/academy/campus/demo3/security/UserNotActivatedException.java
#    create src/main/java/academy/campus/demo3/config/package-info.java
#    create src/main/java/academy/campus/demo3/config/CacheConfiguration.java
#    create src/main/java/academy/campus/demo3/security/SecurityUtils.java
#    create src/main/java/academy/campus/demo3/web/rest/vm/LoginVM.java
#    create src/main/java/academy/campus/demo3/config/AsyncConfiguration.java
#    create src/main/java/academy/campus/demo3/config/DatabaseConfiguration.java
#    create src/main/java/academy/campus/demo3/web/rest/errors/ErrorConstants.java
#    create src/main/java/academy/campus/demo3/security/AuthoritiesConstants.java
#    create src/main/java/academy/campus/demo3/web/rest/UserJWTController.java
#    create src/main/java/academy/campus/demo3/config/DateTimeFormatConfiguration.java
#    create src/main/java/academy/campus/demo3/config/LiquibaseConfiguration.java
#    create src/main/java/academy/campus/demo3/web/rest/errors/ExceptionTranslator.java
#    create src/main/java/academy/campus/demo3/security/package-info.java
#    create src/main/java/academy/campus/demo3/config/LoggingConfiguration.java
#    create src/main/java/academy/campus/demo3/domain/package-info.java
#    create src/main/java/academy/campus/demo3/web/rest/errors/FieldErrorVM.java
#    create src/main/java/academy/campus/demo3/config/ApplicationProperties.java
#    create src/main/java/academy/campus/demo3/domain/AbstractAuditingEntity.java
#    create src/main/java/academy/campus/demo3/web/rest/errors/EmailAlreadyUsedException.java
#    create src/main/java/academy/campus/demo3/security/jwt/TokenProvider.java
#    create src/main/java/academy/campus/demo3/Demo3App.java
#    create src/main/java/academy/campus/demo3/config/JacksonConfiguration.java
#    create src/main/java/academy/campus/demo3/repository/package-info.java
#    create src/main/java/academy/campus/demo3/web/rest/errors/InvalidPasswordException.java
#    create src/main/java/academy/campus/demo3/security/jwt/JWTFilter.java
#    create src/main/java/academy/campus/demo3/ApplicationWebXml.java
#    create src/main/java/academy/campus/demo3/config/LoggingAspectConfiguration.java
#    create src/main/java/academy/campus/demo3/service/EmailAlreadyUsedException.java
#    create src/main/java/academy/campus/demo3/web/rest/errors/LoginAlreadyUsedException.java
#    create src/test/resources/logback.xml
#    create src/main/java/academy/campus/demo3/management/SecurityMetersService.java
#    create src/main/java/academy/campus/demo3/config/WebConfigurer.java
#    create src/main/java/academy/campus/demo3/service/InvalidPasswordException.java
#    create src/main/java/academy/campus/demo3/web/rest/vm/package-info.java
#    create src/test/resources/junit-platform.properties
#    create src/main/java/academy/campus/demo3/security/jwt/JWTConfigurer.java
#    create src/main/java/academy/campus/demo3/config/StaticResourcesWebConfiguration.java
#    create src/main/java/academy/campus/demo3/service/UsernameAlreadyUsedException.java
#    create src/main/java/academy/campus/demo3/web/rest/package-info.java
#    create src/main/java/academy/campus/demo3/config/SecurityConfiguration.java
#    create src/main/java/academy/campus/demo3/GeneratedByJHipster.java
#    create src/main/java/academy/campus/demo3/config/Constants.java
#    create src/main/java/academy/campus/demo3/service/package-info.java
#    create src/main/java/academy/campus/demo3/web/rest/ClientForwardController.java
#    create src/main/java/academy/campus/demo3/security/DomainUserDetailsService.java
#    create src/main/java/academy/campus/demo3/aop/logging/LoggingAspect.java
#    create src/main/java/academy/campus/demo3/config/LocaleConfiguration.java
#    create src/main/java/academy/campus/demo3/web/rest/errors/package-info.java
#    create src/main/java/academy/campus/demo3/web/rest/errors/BadRequestAlertException.java
#    create src/test/java/academy/campus/demo3/security/SecurityUtilsUnitTest.java
#    create src/main/java/academy/campus/demo3/domain/User.java
#    create src/main/java/academy/campus/demo3/domain/Authority.java
#    create src/main/java/academy/campus/demo3/repository/AuthorityRepository.java
#    create src/main/resources/config/liquibase/data/user.csv
#    create src/test/java/academy/campus/demo3/TechnicalStructureTest.java
#    create src/main/resources/config/liquibase/data/authority.csv
#    create src/test/java/academy/campus/demo3/IntegrationTest.java
#    create src/main/resources/config/liquibase/data/user_authority.csv
#    create src/test/java/academy/campus/demo3/web/rest/TestUtil.java
#    create src/test/java/academy/campus/demo3/config/WebConfigurerTest.java
#    create src/test/java/academy/campus/demo3/web/rest/errors/ExceptionTranslatorTestController.java
#    create src/test/java/academy/campus/demo3/config/WebConfigurerTestController.java
#    create src/test/java/academy/campus/demo3/web/rest/errors/ExceptionTranslatorIT.java
#    create src/test/java/academy/campus/demo3/config/StaticResourcesWebConfigurerTest.java
#    create src/test/java/academy/campus/demo3/web/rest/ClientForwardControllerTest.java
#    create src/main/java/academy/campus/demo3/repository/UserRepository.java
#    create src/test/java/academy/campus/demo3/config/timezone/HibernateTimeZoneIT.java
#    create src/test/java/academy/campus/demo3/security/DomainUserDetailsServiceIT.java
#    create src/main/java/academy/campus/demo3/service/UserService.java
#    create src/test/java/academy/campus/demo3/repository/timezone/DateTimeWrapper.java
#    create src/main/java/academy/campus/demo3/service/MailService.java
#    create src/test/java/academy/campus/demo3/repository/timezone/DateTimeWrapperRepository.java
#    create src/test/resources/config/application.yml
#    create src/test/resources/config/application-testcontainers.yml
#    create src/main/java/academy/campus/demo3/web/rest/vm/ManagedUserVM.java
#    create src/main/resources/templates/mail/activationEmail.html
#    create src/main/java/academy/campus/demo3/web/rest/AccountResource.java
#    create src/main/resources/templates/mail/creationEmail.html
#    create src/main/java/academy/campus/demo3/web/rest/UserResource.java
#    create src/main/resources/templates/mail/passwordResetEmail.html
#    create src/main/java/academy/campus/demo3/web/rest/PublicUserResource.java
#    create src/test/resources/config/bootstrap.yml
#    create src/main/java/academy/campus/demo3/web/rest/vm/KeyAndPasswordVM.java
#    create src/main/java/academy/campus/demo3/service/dto/package-info.java
#    create src/main/java/academy/campus/demo3/service/dto/AdminUserDTO.java
#    create src/main/java/academy/campus/demo3/service/dto/UserDTO.java
#    create src/main/java/academy/campus/demo3/service/dto/PasswordChangeDTO.java
#    create src/test/java/academy/campus/demo3/web/rest/UserJWTControllerIT.java
#    create src/main/java/academy/campus/demo3/service/mapper/package-info.java
#    create src/main/java/academy/campus/demo3/service/mapper/UserMapper.java
#    create src/test/java/academy/campus/demo3/management/SecurityMetersServiceTests.java
#    create src/test/java/academy/campus/demo3/config/NoOpMailConfiguration.java
#    create src/test/java/academy/campus/demo3/security/jwt/TokenProviderTest.java
#    create src/test/java/academy/campus/demo3/web/rest/PublicUserResourceIT.java
#    create src/test/java/academy/campus/demo3/security/jwt/TokenProviderSecurityMetersTests.java
#    create src/test/java/academy/campus/demo3/web/rest/UserResourceIT.java
#    create src/test/java/academy/campus/demo3/security/jwt/JWTFilterTest.java
#    create src/test/java/academy/campus/demo3/web/rest/AccountResourceIT.java
#    create src/test/java/academy/campus/demo3/web/rest/WithUnauthenticatedMockUser.java
#    create webpack/logo-jhipster.png
#    create src/test/java/academy/campus/demo3/service/MailServiceIT.java
#    create tsconfig.json
#    create tsconfig.app.json
#    create src/test/java/academy/campus/demo3/service/UserServiceIT.java
#    create tsconfig.spec.json
#    create src/test/java/academy/campus/demo3/service/mapper/UserMapperTest.java
#    create jest.conf.js
#    create .eslintrc.json
#    create angular.json
#    create ngsw-config.json
#    create src/test/resources/templates/mail/testEmail.html
#    create .browserslistrc
#    create webpack/environment.js
#    create webpack/proxy.conf.js
#    create webpack/webpack.custom.js
#    create src/main/webapp/main.ts
#    create src/main/webapp/bootstrap.ts
#    create src/main/webapp/polyfills.ts
#    create src/main/webapp/declarations.d.ts
#    create src/main/webapp/app/app.module.ts
#    create src/main/webapp/content/scss/_bootstrap-variables.scss
#    create src/main/webapp/app/app-routing.module.ts
#    create src/main/webapp/content/scss/global.scss
#    create src/main/webapp/app/app.constants.ts
#    create src/main/webapp/content/scss/vendor.scss
#    create src/main/webapp/app/entities/entity-navbar-items.ts
#    create src/main/webapp/app/entities/entity-routing.module.ts
#    create src/main/webapp/app/home/home.module.ts
#    create src/main/webapp/app/home/home.route.ts
#    create src/main/webapp/app/home/home.component.ts
#    create src/main/webapp/app/home/home.component.html
#    create src/main/webapp/app/home/home.component.scss
#    create src/main/webapp/app/layouts/profiles/page-ribbon.component.ts
#    create src/main/webapp/app/layouts/error/error.component.ts
#    create src/main/webapp/app/layouts/profiles/profile.service.ts
#    create src/main/webapp/app/layouts/error/error.component.html
#    create src/main/webapp/app/layouts/profiles/profile-info.model.ts
#    create src/main/webapp/app/login/login.service.ts
#    create src/main/webapp/app/layouts/main/main.component.ts
#    create src/main/webapp/app/layouts/navbar/active-menu.directive.ts
#    create src/main/webapp/app/layouts/main/main.component.html
#    create src/main/webapp/app/layouts/profiles/page-ribbon.component.scss
#    create src/main/webapp/app/layouts/navbar/navbar.component.ts
#    create src/main/webapp/app/layouts/navbar/navbar.component.scss
#    create src/main/webapp/app/layouts/navbar/navbar.component.html
#    create src/main/webapp/app/layouts/navbar/navbar.route.ts
#    create src/main/webapp/app/login/login.module.ts
#    create src/main/webapp/app/layouts/footer/footer.component.ts
#    create src/main/webapp/app/login/login.route.ts
#    create src/main/webapp/app/layouts/footer/footer.component.html
#    create src/main/webapp/app/login/login.component.ts
#    create src/main/webapp/app/layouts/error/error.route.ts
#    create src/main/webapp/app/login/login.component.html
#    create src/main/webapp/app/login/login.model.ts
#    create src/main/webapp/app/account/account.route.ts
#    create src/main/webapp/app/account/account.module.ts
#    create src/main/webapp/app/account/activate/activate.route.ts
#    create src/main/webapp/app/account/activate/activate.component.ts
#    create src/main/webapp/app/account/activate/activate.component.html
#    create src/main/webapp/app/account/activate/activate.service.ts
#    create src/main/webapp/app/account/password/password.route.ts
#    create src/main/webapp/app/account/password/password-strength-bar/password-strength-bar.component.ts
#    create src/main/webapp/app/account/password/password-strength-bar/password-strength-bar.component.html
#    create src/main/webapp/app/account/password/password-strength-bar/password-strength-bar.component.scss
#    create src/main/webapp/app/account/password/password.component.ts
#    create src/main/webapp/app/account/password/password.component.html
#    create src/main/webapp/app/account/password/password.service.ts
#    create src/main/webapp/app/account/register/register.route.ts
#    create src/main/webapp/app/account/register/register.component.ts
#    create src/main/webapp/app/account/register/register.component.html
#    create src/main/webapp/app/account/register/register.service.ts
#    create src/main/webapp/app/account/register/register.model.ts
#    create src/main/webapp/app/account/settings/settings.route.ts
#    create src/main/webapp/app/account/settings/settings.component.ts
#    create src/main/webapp/app/account/settings/settings.component.html
#    create src/main/webapp/app/account/password-reset/init/password-reset-init.route.ts
#    create src/main/webapp/app/admin/admin-routing.module.ts
#    create src/main/webapp/app/account/password-reset/init/password-reset-init.component.ts
#    create src/main/webapp/app/admin/docs/docs.route.ts
#    create src/main/webapp/app/account/password-reset/init/password-reset-init.component.html
#    create src/main/webapp/app/admin/docs/docs.module.ts
#    create src/main/webapp/app/account/password-reset/init/password-reset-init.service.ts
#    create src/main/webapp/app/admin/docs/docs.component.ts
#    create src/main/webapp/app/account/password-reset/finish/password-reset-finish.route.ts
#    create src/main/webapp/app/admin/docs/docs.component.html
#    create src/main/webapp/app/account/password-reset/finish/password-reset-finish.component.ts
#    create src/main/webapp/app/admin/docs/docs.component.scss
#    create src/main/webapp/app/account/password-reset/finish/password-reset-finish.component.html
#    create src/main/webapp/app/admin/configuration/configuration.route.ts
#    create src/main/webapp/app/account/password-reset/finish/password-reset-finish.service.ts
#    create src/main/webapp/app/admin/configuration/configuration.module.ts
#    create src/main/webapp/app/admin/configuration/configuration.component.ts
#    create src/main/webapp/app/admin/configuration/configuration.component.html
#    create src/main/webapp/app/admin/configuration/configuration.service.ts
#    create src/main/webapp/app/admin/configuration/configuration.model.ts
#    create src/main/webapp/app/admin/health/health.route.ts
#    create src/main/webapp/app/admin/health/health.module.ts
#    create src/main/webapp/app/admin/health/health.component.ts
#    create src/main/webapp/app/admin/health/health.component.html
#    create src/main/webapp/app/admin/health/modal/health-modal.component.ts
#    create src/main/webapp/app/admin/health/modal/health-modal.component.html
#    create src/main/webapp/app/admin/health/health.service.ts
#    create src/main/webapp/app/admin/health/health.model.ts
#    create src/main/webapp/app/admin/logs/logs.route.ts
#    create src/main/webapp/app/admin/logs/logs.module.ts
#    create src/main/webapp/app/admin/logs/log.model.ts
#    create src/main/webapp/app/admin/logs/logs.component.ts
#    create src/main/webapp/app/admin/logs/logs.component.html
#    create src/main/webapp/app/admin/logs/logs.service.ts
#    create src/main/webapp/app/admin/metrics/metrics.route.ts
#    create src/main/webapp/app/admin/metrics/metrics.module.ts
#    create src/main/webapp/app/admin/metrics/metrics.component.ts
#    create src/main/webapp/app/admin/metrics/metrics.component.html
#    create src/main/webapp/app/admin/metrics/metrics.service.ts
#    create src/main/webapp/app/admin/metrics/metrics.model.ts
#    create src/main/webapp/app/admin/metrics/blocks/jvm-memory/jvm-memory.component.ts
#    create src/main/webapp/app/admin/metrics/blocks/metrics-garbagecollector/metrics-garbagecollector.component.html
#    create src/main/webapp/app/admin/metrics/blocks/jvm-memory/jvm-memory.component.html
#    create src/main/webapp/app/admin/metrics/blocks/metrics-modal-threads/metrics-modal-threads.component.ts
#    create src/main/webapp/app/admin/metrics/blocks/jvm-threads/jvm-threads.component.ts
#    create src/main/webapp/app/admin/metrics/blocks/metrics-modal-threads/metrics-modal-threads.component.html
#    create src/main/webapp/app/admin/metrics/blocks/jvm-threads/jvm-threads.component.html
#    create src/main/webapp/app/admin/metrics/blocks/metrics-request/metrics-request.component.ts
#    create src/main/webapp/app/admin/metrics/blocks/metrics-cache/metrics-cache.component.ts
#    create src/main/webapp/app/admin/metrics/blocks/metrics-request/metrics-request.component.html
#    create src/main/webapp/app/admin/metrics/blocks/metrics-cache/metrics-cache.component.html
#    create src/main/webapp/app/admin/metrics/blocks/metrics-system/metrics-system.component.ts
#    create src/main/webapp/app/admin/metrics/blocks/metrics-datasource/metrics-datasource.component.ts
#    create src/main/webapp/app/admin/metrics/blocks/metrics-system/metrics-system.component.html
#    create src/main/webapp/app/admin/metrics/blocks/metrics-datasource/metrics-datasource.component.html
#    create src/main/webapp/app/admin/user-management/user-management.route.ts
#    create src/main/webapp/app/admin/metrics/blocks/metrics-endpoints-requests/metrics-endpoints-requests.component.ts
#    create src/main/webapp/app/admin/user-management/user-management.module.ts
#    create src/main/webapp/app/admin/metrics/blocks/metrics-endpoints-requests/metrics-endpoints-requests.component.html
#    create src/main/webapp/app/admin/user-management/user-management.model.ts
#    create src/main/webapp/app/admin/metrics/blocks/metrics-garbagecollector/metrics-garbagecollector.component.ts
#    create src/main/webapp/app/admin/user-management/list/user-management.component.ts
#    create src/main/webapp/app/admin/user-management/list/user-management.component.html
#    create src/main/webapp/app/admin/user-management/detail/user-management-detail.component.ts
#    create src/main/webapp/app/admin/user-management/detail/user-management-detail.component.html
#    create src/main/webapp/app/admin/user-management/update/user-management-update.component.ts
#    create src/main/webapp/app/admin/user-management/update/user-management-update.component.html
#    create src/main/webapp/app/admin/user-management/delete/user-management-delete-dialog.component.ts
#    create src/main/webapp/app/admin/user-management/delete/user-management-delete-dialog.component.html
#    create src/main/webapp/app/admin/user-management/service/user-management.service.ts
#    create src/main/webapp/app/config/uib-pagination.config.ts
#    create src/main/webapp/app/config/dayjs.ts
#    create src/main/webapp/app/config/datepicker-adapter.ts
#    create src/main/webapp/app/core/config/application-config.service.ts
#    create src/main/webapp/app/config/font-awesome-icons.ts
#    create src/main/webapp/app/core/config/application-config.service.spec.ts
#    create src/main/webapp/app/config/error.constants.ts
#    create src/main/webapp/app/core/util/data-util.service.ts
#    create src/main/webapp/app/config/input.constants.ts
#    create src/main/webapp/app/core/util/parse-links.service.ts
#    create src/main/webapp/app/config/pagination.constants.ts
#    create src/main/webapp/app/core/util/alert.service.ts
#    create src/main/webapp/app/config/authority.constants.ts
#    create src/main/webapp/app/config/language.constants.ts
#    create src/main/webapp/app/core/util/event-manager.service.ts
#    create src/main/webapp/app/core/interceptor/error-handler.interceptor.ts
#    create src/main/webapp/app/config/translation.config.ts
#    create src/main/webapp/app/core/util/operators.spec.ts
#    create src/main/webapp/app/core/interceptor/notification.interceptor.ts
#    create src/main/webapp/app/core/util/operators.ts
#    create src/main/webapp/app/core/interceptor/auth-expired.interceptor.ts
#    create src/main/webapp/app/core/interceptor/index.ts
#    create src/main/webapp/app/core/request/request-util.ts
#    create src/main/webapp/app/core/request/request.model.ts
#    create src/main/webapp/app/core/interceptor/auth.interceptor.ts
#    create src/main/webapp/app/entities/user/user.service.ts
#    create src/main/webapp/app/entities/user/user.service.spec.ts
#    create src/main/webapp/app/entities/user/user.model.ts
#    create src/main/webapp/app/shared/shared.module.ts
#    create src/main/webapp/app/shared/shared-libs.module.ts
#    create src/main/webapp/app/shared/date/duration.pipe.ts
#    create src/main/webapp/app/shared/date/format-medium-date.pipe.ts
#    create src/main/webapp/app/shared/date/format-medium-datetime.pipe.ts
#    create src/main/webapp/app/shared/sort/sort.directive.ts
#    create src/main/webapp/app/shared/sort/sort-by.directive.ts
#    create src/main/webapp/app/shared/pagination/item-count.component.ts
#    create src/main/webapp/app/shared/alert/alert.component.ts
#    create src/main/webapp/app/shared/alert/alert.component.html
#    create src/main/webapp/app/shared/alert/alert-error.component.ts
#    create src/main/webapp/app/shared/alert/alert-error.component.html
#    create src/main/webapp/app/shared/alert/alert-error.model.ts
#    create src/main/webapp/app/shared/language/translation.module.ts
#    create src/main/webapp/app/admin/configuration/configuration.component.spec.ts
#    create src/main/webapp/app/shared/language/find-language-from-key.pipe.ts
#    create src/main/webapp/app/admin/configuration/configuration.service.spec.ts
#    create src/main/webapp/app/shared/language/translate.directive.ts
#    create src/main/webapp/app/admin/health/modal/health-modal.component.spec.ts
#    create src/main/webapp/app/core/auth/state-storage.service.ts
#    create src/main/webapp/app/admin/health/health.component.spec.ts
#    create src/main/webapp/app/shared/auth/has-any-authority.directive.ts
#    create src/main/webapp/app/admin/health/health.service.spec.ts
#    create src/main/webapp/app/core/auth/account.model.ts
#    create src/main/webapp/app/admin/logs/logs.component.spec.ts
#    create src/main/webapp/app/core/auth/account.service.ts
#    create src/main/webapp/app/admin/logs/logs.service.spec.ts
#    create src/main/webapp/app/core/auth/account.service.spec.ts
#    create src/main/webapp/app/admin/metrics/metrics.component.spec.ts
#    create src/main/webapp/app/core/auth/user-route-access.service.ts
#    create src/main/webapp/app/admin/metrics/metrics.service.spec.ts
#    create src/main/webapp/app/core/auth/auth-jwt.service.ts
#    create src/main/webapp/app/shared/auth/has-any-authority.directive.spec.ts
#    create src/main/webapp/app/core/auth/auth-jwt.service.spec.ts
#    create src/main/webapp/app/core/util/event-manager.service.spec.ts
#    create src/main/webapp/app/core/util/data-util.service.spec.ts
#    create src/main/webapp/app/core/util/parse-links.service.spec.ts
#    create src/main/webapp/app/core/util/alert.service.spec.ts
#    create src/main/webapp/app/home/home.component.spec.ts
#    create src/main/webapp/app/layouts/main/main.component.spec.ts
#    create src/main/webapp/app/layouts/navbar/navbar.component.spec.ts
#    create src/main/webapp/app/layouts/profiles/page-ribbon.component.spec.ts
#    create src/main/webapp/app/shared/alert/alert.component.spec.ts
#    create src/main/webapp/app/shared/alert/alert-error.component.spec.ts
#    create src/main/webapp/app/shared/date/format-medium-date.pipe.spec.ts
#    create src/main/webapp/app/shared/date/format-medium-datetime.pipe.spec.ts
#    create src/main/webapp/app/shared/sort/sort.directive.spec.ts
#    create src/main/webapp/app/shared/sort/sort-by.directive.spec.ts
#    create src/main/webapp/app/shared/pagination/item-count.component.spec.ts
#    create src/main/webapp/app/shared/language/translate.directive.spec.ts
#    create src/main/webapp/app/account/activate/activate.component.spec.ts
#    create src/main/webapp/app/account/activate/activate.service.spec.ts
#    create src/main/webapp/app/account/password/password.component.spec.ts
#    create src/main/webapp/app/account/password/password.service.spec.ts
#    create src/main/webapp/app/account/password/password-strength-bar/password-strength-bar.component.spec.ts
#    create src/main/webapp/app/account/password-reset/init/password-reset-init.component.spec.ts
#    create src/main/webapp/app/account/password-reset/init/password-reset-init.service.spec.ts
#    create src/main/webapp/app/account/password-reset/finish/password-reset-finish.component.spec.ts
#    create src/main/webapp/app/account/password-reset/finish/password-reset-finish.service.spec.ts
#    create src/main/webapp/app/account/register/register.component.spec.ts
#    create src/main/webapp/app/account/register/register.service.spec.ts
#    create src/main/webapp/app/account/settings/settings.component.spec.ts
#    create src/main/webapp/app/login/login.component.spec.ts
#    create src/main/webapp/app/admin/user-management/list/user-management.component.spec.ts
#    create src/main/webapp/app/admin/user-management/detail/user-management-detail.component.spec.ts
#    create src/main/webapp/app/admin/user-management/update/user-management-update.component.spec.ts
#    create src/main/webapp/app/admin/user-management/delete/user-management-delete-dialog.component.spec.ts
#    create src/main/webapp/app/admin/user-management/service/user-management.service.spec.ts
#    create src/main/webapp/content/images/jhipster_family_member_0.svg
#    create src/main/webapp/content/images/jhipster_family_member_0_head-192.png
#    create src/main/webapp/content/images/jhipster_family_member_0_head-256.png
#    create src/main/webapp/content/images/jhipster_family_member_0_head-384.png
#    create src/main/webapp/content/images/jhipster_family_member_0_head-512.png
#    create src/main/webapp/content/images/jhipster_family_member_1.svg
#    create src/main/webapp/content/images/jhipster_family_member_1_head-192.png
#    create src/main/webapp/content/images/jhipster_family_member_1_head-256.png
#    create src/main/webapp/content/images/jhipster_family_member_1_head-384.png
#    create src/main/webapp/content/images/jhipster_family_member_1_head-512.png
#    create src/main/webapp/content/images/jhipster_family_member_2.svg
#    create src/main/webapp/content/images/jhipster_family_member_2_head-192.png
#    create src/main/webapp/content/images/jhipster_family_member_2_head-256.png
#    create src/main/webapp/content/images/jhipster_family_member_2_head-384.png
#    create src/main/webapp/content/images/jhipster_family_member_2_head-512.png
#    create src/main/webapp/content/images/jhipster_family_member_3.svg
#    create src/main/webapp/content/images/jhipster_family_member_3_head-192.png
#    create src/main/webapp/content/images/jhipster_family_member_3_head-256.png
#    create src/main/webapp/content/images/jhipster_family_member_3_head-384.png
#    create src/main/webapp/content/images/jhipster_family_member_3_head-512.png
#    create src/main/webapp/content/images/logo-jhipster.png
#    create src/main/webapp/favicon.ico
#    create src/main/webapp/swagger-ui/dist/images/throbber.gif
#    create .eslintignore
#    create src/main/webapp/manifest.webapp
#    create src/main/webapp/WEB-INF/web.xml
#    create src/main/webapp/robots.txt
#    create src/main/webapp/404.html
#    create src/main/webapp/content/css/loading.css
#    create src/main/webapp/index.html
#    create src/main/webapp/swagger-ui/index.html
#    create src/main/webapp/i18n/en/error.json
#    create src/main/webapp/i18n/en/configuration.json
#    create src/main/webapp/i18n/en/logs.json
#    create src/main/webapp/i18n/en/metrics.json
#    create src/main/webapp/i18n/en/login.json
#    create src/main/webapp/i18n/en/activate.json
#    create src/main/webapp/i18n/en/home.json
#    create src/main/webapp/i18n/en/password.json
#    create src/main/webapp/i18n/en/global.json
#    create src/main/webapp/i18n/en/register.json
#    create src/main/webapp/i18n/en/reset.json
#    create src/main/webapp/i18n/en/sessions.json
#    create src/main/webapp/i18n/en/health.json
#    create src/main/webapp/i18n/en/settings.json
#    create src/main/webapp/i18n/fr/error.json
#    create src/main/webapp/i18n/en/user-management.json
#    create src/main/webapp/i18n/fr/settings.json
#    create src/main/webapp/i18n/fr/login.json
#    create src/main/webapp/i18n/fr/user-management.json
#    create src/main/webapp/i18n/fr/home.json
#    create src/main/webapp/i18n/fr/configuration.json
#    create src/main/webapp/i18n/fr/password.json
#    create src/main/webapp/i18n/fr/logs.json
#    create src/main/webapp/i18n/fr/register.json
#    create src/main/webapp/i18n/fr/metrics.json
#    create src/main/webapp/i18n/fr/sessions.json
#    create src/main/webapp/i18n/fr/activate.json
#    create src/main/webapp/i18n/fr/global.json
#    create src/main/webapp/i18n/fr/reset.json
#    create src/main/webapp/i18n/fr/health.json
#    create src/main/resources/i18n/messages_en.properties
#    create src/test/resources/i18n/messages_en.properties
#    create src/main/resources/i18n/messages_fr.properties
#    create src/test/resources/i18n/messages_fr.properties
#    create README.md
#     force .yo-rc.json
# Git repository initialized.
# 
# Changes to package.json were detected.
# [INFO] Scanning for projects...
# [INFO] 
# [INFO] --------------------< academy.campus.demo3:demo-3 >---------------------
# [INFO] Building Demo 3 0.0.1-SNAPSHOT
# [INFO] --------------------------------[ jar ]---------------------------------
# [INFO] 
# [INFO] --- frontend-maven-plugin:1.12.1:install-node-and-npm (install-node-and-npm) @ demo-3 ---
# [INFO] Installing node version v16.14.0
# [INFO] Unpacking /home/pierre/.m2/repository/com/github/eirslett/node/16.14.0/node-16.14.0-linux-x64.tar.gz into /home/pierre/workspace/tutorial-jhipster-docker-demo3/target/node/tmp
# [INFO] Copying node binary from /home/pierre/workspace/tutorial-jhipster-docker-demo3/target/node/tmp/node-v16.14.0-linux-x64/bin/node to /home/pierre/workspace/tutorial-jhipster-docker-demo3/target/node/node
# [INFO] Installed node locally.
# [INFO] Installing npm version 8.6.0
# [INFO] Unpacking /home/pierre/.m2/repository/com/github/eirslett/npm/8.6.0/npm-8.6.0.tar.gz into /home/pierre/workspace/tutorial-jhipster-docker-demo3/target/node/node_modules
# [INFO] Installed npm locally.
# [INFO] ------------------------------------------------------------------------
# [INFO] BUILD SUCCESS
# [INFO] ------------------------------------------------------------------------
# [INFO] Total time:  4.397 s
# [INFO] Finished at: 2022-07-08T00:25:26+02:00
# [INFO] ------------------------------------------------------------------------
# Using node installed locally v16.14.0
# Using npm installed locally 8.6.0
# npm WARN deprecated source-map-resolve@0.6.0: See https://github.com/lydell/source-map-resolve#deprecated
# npm WARN deprecated har-validator@5.1.5: this library is no longer supported
# npm WARN deprecated querystring@0.2.0: The querystring API is considered Legacy. new code should use the URLSearchParams API instead.
# npm WARN deprecated uuid@3.4.0: Please upgrade  to version 7 or higher.  Older versions may use Math.random() in certain circumstances, which is known to be problematic.  See https://v8.dev/blog/math-random for details.
# npm WARN deprecated uuid@3.3.2: Please upgrade  to version 7 or higher.  Older versions may use Math.random() in certain circumstances, which is known to be problematic.  See https://v8.dev/blog/math-random for details.
# npm WARN deprecated request@2.88.2: request has been deprecated, see https://github.com/request/request/issues/3142
# npm WARN deprecated core-js@3.20.3: core-js@<3.23.3 is no longer maintained and not recommended for usage due to the number of issues. Because of the V8 engine whims, feature detection in old core-js versions could cause a slowdown up to 100x even if nothing is polyfilled. Some versions have web compatibility issues. Please, upgrade your dependencies to the actual version of core-js.
# 
# > demo-3@0.0.1-SNAPSHOT prepare
# > husky install
# 
# husky - Git hooks installed
# 
# added 1813 packages, and audited 1814 packages in 2m
# 
# 185 packages are looking for funding
#   run `npm fund` for details
# 
# 2 critical severity vulnerabilities
# 
# To address all issues (including breaking changes), run:
#   npm audit fix --force
# 
# Run `npm audit` for details.
# Application successfully committed to Git from /home/pierre/workspace/tutorial-jhipster-docker-demo3.
# 
# If you find JHipster useful consider sponsoring the project https://www.jhipster.tech/sponsors/
# 
# Server application generated successfully.
# 
# Run your Spring Boot application:
# ./mvnw
# 
# Client application generated successfully.
# 
# Start your Webpack development server with:
#  npm start
# 
# 
# > demo-3@0.0.1-SNAPSHOT clean-www /home/pierre/workspace/tutorial-jhipster-docker-demo3
# > rimraf target/classes/static/app/{src,target/}
# 
# Congratulations, JHipster execution is complete!
# Sponsored with ❤️  by @oktadev.

### Ceinture et bretelles : s'assurer que les dépendances npm sont bien installées (sous ./node_modules)
./npmw install
# Using node installed locally v16.14.0
# Using npm installed locally 8.6.0
# 
# > demo-3@0.0.1-SNAPSHOT prepare
# > husky install
# 
# husky - Git hooks installed
# 
# up to date, audited 1814 packages in 6s
# 
# 185 packages are looking for funding
#   run `npm fund` for details
# 
# 2 critical severity vulnerabilities
# 
# To address all issues (including breaking changes), run:
#   npm audit fix --force
# 
# Run `npm audit` for details.

### S'assurer que l'appli par défaut est OK
./mvnw
# [INFO] Scanning for projects...
# [INFO] 
# [INFO] --------------------< academy.campus.demo3:demo-3 >---------------------
# [INFO] Building Demo 3 0.0.1-SNAPSHOT
# [INFO] --------------------------------[ jar ]---------------------------------
# [INFO] 
# [INFO] >>> spring-boot-maven-plugin:2.6.6:run (default-cli) > test-compile @ demo-3 >>>
# [INFO] 
# [INFO] --- maven-resources-plugin:3.2.0:copy-resources (default-resources) @ demo-3 ---
# [INFO] Using 'UTF-8' encoding to copy filtered resources.
# [INFO] Using 'UTF-8' encoding to copy filtered properties files.
# [INFO] Copying 6 resources
# [INFO] Copying 16 resources
# [INFO] 
# [INFO] --- maven-resources-plugin:3.2.0:resources (default-resources) @ demo-3 ---
# [INFO] Using 'UTF-8' encoding to copy filtered resources.
# [INFO] Using 'UTF-8' encoding to copy filtered properties files.
# [INFO] Copying 6 resources
# [INFO] Copying 16 resources
# [INFO] 
# [INFO] --- maven-enforcer-plugin:3.0.0-M3:enforce (enforce-versions) @ demo-3 ---
# [INFO] 
# [INFO] --- maven-enforcer-plugin:3.0.0-M3:enforce (enforce-dependencyConvergence) @ demo-3 ---
# [WARNING] 
# Dependency convergence error for org.apiguardian:apiguardian-api:1.1.2 paths to dependency are:
# +-academy.campus.demo3:demo-3:0.0.1-SNAPSHOT
#   +-org.springframework.boot:spring-boot-starter-test:2.6.6
#     +-org.junit.jupiter:junit-jupiter:5.8.2
#       +-org.junit.jupiter:junit-jupiter-api:5.8.2
#         +-org.junit.platform:junit-platform-commons:1.8.2
#           +-org.apiguardian:apiguardian-api:1.1.2
# and
# +-academy.campus.demo3:demo-3:0.0.1-SNAPSHOT
#   +-org.springframework.boot:spring-boot-starter-test:2.6.6
#     +-org.junit.jupiter:junit-jupiter:5.8.2
#       +-org.junit.jupiter:junit-jupiter-api:5.8.2
#         +-org.apiguardian:apiguardian-api:1.1.2
# and
# +-academy.campus.demo3:demo-3:0.0.1-SNAPSHOT
#   +-org.springframework.boot:spring-boot-starter-test:2.6.6
#     +-org.junit.jupiter:junit-jupiter:5.8.2
#       +-org.junit.jupiter:junit-jupiter-params:5.8.2
#         +-org.apiguardian:apiguardian-api:1.1.2
# and
# +-academy.campus.demo3:demo-3:0.0.1-SNAPSHOT
#   +-org.springframework.boot:spring-boot-starter-test:2.6.6
#     +-org.junit.jupiter:junit-jupiter:5.8.2
#       +-org.junit.jupiter:junit-jupiter-engine:5.8.2
#         +-org.apiguardian:apiguardian-api:1.1.2
# and
# +-academy.campus.demo3:demo-3:0.0.1-SNAPSHOT
#   +-com.tngtech.archunit:archunit-junit5-engine:0.22.0
#     +-com.tngtech.archunit:archunit-junit5-engine-api:0.22.0
#       +-org.junit.platform:junit-platform-engine:1.8.2
#         +-org.apiguardian:apiguardian-api:1.1.2
# and
# +-academy.campus.demo3:demo-3:0.0.1-SNAPSHOT
#   +-org.zalando:problem-spring-web:0.27.0
#     +-org.zalando:problem-violations:0.27.0
#       +-org.apiguardian:apiguardian-api:1.1.2
# and
# +-academy.campus.demo3:demo-3:0.0.1-SNAPSHOT
#   +-org.zalando:problem-spring-web:0.27.0
#     +-org.zalando:problem-spring-common:0.27.0
#       +-org.apiguardian:apiguardian-api:1.1.2
# and
# +-academy.campus.demo3:demo-3:0.0.1-SNAPSHOT
#   +-org.zalando:problem-spring-web:0.27.0
#     +-org.apiguardian:apiguardian-api:1.1.2
# and
# +-academy.campus.demo3:demo-3:0.0.1-SNAPSHOT
#   +-org.zalando:problem-spring-web:0.27.0
#     +-org.zalando:faux-pas:0.9.0
#       +-org.apiguardian:apiguardian-api:1.1.1
# 
# [WARNING] Rule 0: org.apache.maven.plugins.enforcer.DependencyConvergence failed with message:
# Failed while enforcing releasability. See above detailed error message.
# [INFO] 
# [INFO] --- jacoco-maven-plugin:0.8.7:prepare-agent (pre-unit-tests) @ demo-3 ---
# [INFO] argLine set to -javaagent:/home/pierre/.m2/repository/org/jacoco/org.jacoco.agent/0.8.7/org.jacoco.agent-0.8.7-runtime.jar=destfile=/home/pierre/workspace/tutorial-jhipster-docker-demo3/target/jacoco.exec -Djava.security.egd=file:/dev/./urandom -Xmx512m
# [INFO] 
# [INFO] --- properties-maven-plugin:1.1.0:read-project-properties (default) @ demo-3 ---
# [INFO] 
# [INFO] --- checksum-maven-plugin:1.11:files (create-pre-compiled-webapp-checksum) @ demo-3 ---
# [INFO] 
# [INFO] --- maven-antrun-plugin:3.0.0:run (eval-frontend-checksum) @ demo-3 ---
# [INFO] Executing tasks
# [INFO] Executed tasks
# [INFO] 
# [INFO] --- frontend-maven-plugin:1.12.1:install-node-and-npm (install-node-and-npm) @ demo-3 ---
# [INFO] Node v16.14.0 is already installed.
# [INFO] NPM 8.6.0 is already installed.
# [INFO] 
# [INFO] --- frontend-maven-plugin:1.12.1:npm (npm install) @ demo-3 ---
# [INFO] Running 'npm install' in /home/pierre/workspace/tutorial-jhipster-docker-demo3
# [INFO] 
# [INFO] > demo-3@0.0.1-SNAPSHOT prepare
# [INFO] > husky install
# [INFO] 
# [INFO] husky - Git hooks installed
# [INFO] 
# [INFO] up to date, audited 1814 packages in 5s
# [INFO] 
# [INFO] 185 packages are looking for funding
# [INFO]   run `npm fund` for details
# [INFO] 
# [INFO] 2 critical severity vulnerabilities
# [INFO] 
# [INFO] To address all issues (including breaking changes), run:
# [INFO]   npm audit fix --force
# [INFO] 
# [INFO] Run `npm audit` for details.
# [INFO] 
# [INFO] --- frontend-maven-plugin:1.12.1:npm (webapp build dev) @ demo-3 ---
# [INFO] npm not inheriting proxy config from Maven
# [INFO] Running 'npm run webapp:build' in /home/pierre/workspace/tutorial-jhipster-docker-demo3
# [INFO] 
# [INFO] > demo-3@0.0.1-SNAPSHOT webapp:build
# [INFO] > npm run clean-www && npm run webapp:build:dev
# [INFO] 
# [INFO] 
# [INFO] > demo-3@0.0.1-SNAPSHOT clean-www
# [INFO] > rimraf target/classes/static/app/{src,target/}
# [INFO] 
# [INFO] 
# [INFO] > demo-3@0.0.1-SNAPSHOT webapp:build:dev
# [INFO] > ng build --configuration development
# [INFO] 
# [INFO] - Generating browser application bundles (phase: setup)...
# [INFO] ✔ Browser application bundle generation complete.
# [INFO] ✔ Browser application bundle generation complete.
# [INFO] - Copying assets...
# [INFO] ✔ Copying assets complete.
# [INFO] - Generating index html...
# [INFO] ✔ Index html generation complete.
# [INFO] 
# [INFO] Initial Chunk Files                                                    | Names                                  |  Raw Size
# [INFO] styles.css                                                             | styles                                 | 205.42 kB |
# [INFO] polyfills.js                                                           | polyfills                              | 134.22 kB |
# [INFO] runtime.js                                                             | runtime                                |  12.45 kB |
# [INFO] main.js                                                                | main                                   | 776 bytes |
# [INFO] 
# [INFO] | Initial Total                          | 352.85 kB
# [INFO] 
# [INFO] Lazy Chunk Files                                                       | Names                                  |  Raw Size
# [INFO] 
# [INFO] src_main_webapp_bootstrap_ts.js                                        | bootstrap                              |   4.83 MB |
# [INFO] ./src/main/webapp/content/scss/vendor.scss.webpack[javascript/auto]!=!./node_modules/css-loader/dist/cjs.js??ruleSet[1].rules[6].rules[0].oneOf[0].use[1]!./node_modules/postcss-loader/dist/cjs.js??ruleSet[1].rules[6].rules[0].oneOf[0].use[2]!./node_modules/resolve-url-loader/index.js??ruleSet[1].rules[6].rules[1].use[0]!./node_modules/sass-loader/dist/cjs.js??ruleSet[1].rules[6].rules[1].use[1]!./src/main/webapp/content/scss/vendor.scss - Warning: Module Warning (from ./node_modules/postcss-loader/dist/cjs.js):
# [INFO] src_main_webapp_app_account_account_module_ts.js                       | account-account-module                 | 179.59 kB |
# [INFO] Warning
# [INFO] src_main_webapp_app_admin_metrics_metrics_module_ts.js                 | metrics-metrics-module                 | 173.86 kB |
# [INFO] 
# [INFO] src_main_webapp_app_admin_user-management_user-management_module_ts.js | user-management-user-management-module | 117.81 kB |
# [INFO] (2433:3) autoprefixer: Replace color-adjust to print-color-adjust. The color-adjust shorthand is currently deprecated.
# [INFO] src_main_webapp_app_admin_health_health_module_ts.js                   | health-health-module                   |  30.11 kB |
# [INFO] 
# [INFO] src_main_webapp_app_admin_configuration_configuration_module_ts.js     | configuration-configuration-module     |  25.87 kB |
# [INFO] 
# [INFO] src_main_webapp_app_admin_logs_logs_module_ts.js                       | logs-logs-module                       |  24.08 kB |
# [INFO] src_main_webapp_app_login_login_module_ts.js                           | login-login-module                     |  17.75 kB |
# [INFO] src_main_webapp_app_admin_docs_docs_module_ts.js                       | docs-docs-module                       |   4.71 kB |
# [INFO] src_main_webapp_app_admin_admin-routing_module_ts.js                   | admin-admin-routing-module             |   4.01 kB |
# [INFO] src_main_webapp_app_entities_entity-routing_module_ts.js               | entities-entity-routing-module         |   1.81 kB |
# [INFO] 
# [INFO] Build at: 2022-07-07T22:35:28.649Z - Hash: 27ecc24565ec8d98 - Time: 44442ms
# [INFO] 
# [INFO] --- maven-compiler-plugin:3.10.1:compile (default-compile) @ demo-3 ---
# Downloading from central: https://repo.maven.apache.org/maven2/com/sun/istack/istack-commons-runtime/3.0.7/istack-commons-runtime-3.0.7.jar
# [...]
# Downloaded from central: https://repo.maven.apache.org/maven2/com/google/guava/guava/28.2-android/guava-28.2-android.jar (2.6 MB at 1.5 MB/s)
# [INFO] Attaching agents: []
# 00:35:43.421 [Thread-0] DEBUG org.springframework.boot.devtools.restart.classloader.RestartClassLoader - Created RestartClassLoader org.springframework.boot.devtools.restart.classloader.RestartClassLoader@450769e6
# 
#         ██╗ ██╗   ██╗ ████████╗ ███████╗   ██████╗ ████████╗ ████████╗ ███████╗
#         ██║ ██║   ██║ ╚══██╔══╝ ██╔═══██╗ ██╔════╝ ╚══██╔══╝ ██╔═════╝ ██╔═══██╗
#         ██║ ████████║    ██║    ███████╔╝ ╚█████╗     ██║    ██████╗   ███████╔╝
#   ██╗   ██║ ██╔═══██║    ██║    ██╔════╝   ╚═══██╗    ██║    ██╔═══╝   ██╔══██║
#   ╚██████╔╝ ██║   ██║ ████████╗ ██║       ██████╔╝    ██║    ████████╗ ██║  ╚██╗
#    ╚═════╝  ╚═╝   ╚═╝ ╚═══════╝ ╚═╝       ╚═════╝     ╚═╝    ╚═══════╝ ╚═╝   ╚═╝
# 
# :: JHipster 🤓  :: Running Spring Boot 2.6.6 ::
# :: https://www.jhipster.tech ::
# 
# 2022-07-08 00:35:44.871  INFO 40373 --- [  restartedMain] academy.campus.demo3.Demo3App            : The following 2 profiles are active: "dev", "api-docs"
# 2022-07-08 00:35:47.442 DEBUG 40373 --- [  restartedMain] jdk.event.security                       : X509Certificate: Alg:SHA1withDSA, Serial:10, Subject:CN=JCE Code Signing CA, OU=Java Software Code Signing, O=Sun Microsystems Inc, L=Palo Alto, ST=CA, C=US, Issuer:CN=JCE Code Signing CA, OU=Java Software Code Signing, O=Sun Microsystems Inc, L=Palo Alto, ST=CA, C=US, Key type:DSA, Length:1024, Cert Id:1776909028, Valid from:4/25/01, 9:00 AM, Valid until:4/25/20, 9:00 AM
# 2022-07-08 00:35:47.444 DEBUG 40373 --- [  restartedMain] jdk.event.security                       : X509Certificate: Alg:SHA1withDSA, Serial:47f, Subject:CN=Legion of the Bouncy Castle Inc., OU=Java Software Code Signing, O=Sun Microsystems Inc, Issuer:CN=JCE Code Signing CA, OU=Java Software Code Signing, O=Sun Microsystems Inc, L=Palo Alto, ST=CA, C=US, Key type:DSA, Length:1024, Cert Id:-2023852845, Valid from:3/11/17, 2:15 AM, Valid until:4/25/20, 9:00 AM
# 2022-07-08 00:35:47.462 DEBUG 40373 --- [  restartedMain] jdk.event.security                       : X509Certificate: Alg:SHA1withRSA, Serial:3019a023aff58b16bd6d5eae617f066, Subject:CN=DigiCert Timestamp Responder, O=DigiCert, C=US, Issuer:CN=DigiCert Assured ID CA-1, OU=www.digicert.com, O=DigiCert Inc, C=US, Key type:RSA, Length:2048, Cert Id:-499520198, Valid from:10/22/14, 2:00 AM, Valid until:10/22/24, 2:00 AM
# 2022-07-08 00:35:47.464 DEBUG 40373 --- [  restartedMain] jdk.event.security                       : X509Certificate: Alg:SHA1withRSA, Serial:6fdf9039603adea000aeb3f27bbba1b, Subject:CN=DigiCert Assured ID CA-1, OU=www.digicert.com, O=DigiCert Inc, C=US, Issuer:CN=DigiCert Assured ID Root CA, OU=www.digicert.com, O=DigiCert Inc, C=US, Key type:RSA, Length:2048, Cert Id:-1111363278, Valid from:11/10/06, 1:00 AM, Valid until:11/10/21, 1:00 AM
# 2022-07-08 00:35:47.510 DEBUG 40373 --- [  restartedMain] jdk.event.security                       : X509Certificate: Alg:SHA256withRSA, Serial:3c9eb1fc89f733d3, Subject:CN=JCE Code Signing CA, OU=Java Software Code Signing, O=Oracle Corporation, Issuer:CN=JCE Code Signing CA, OU=Java Software Code Signing, O=Oracle Corporation, Key type:RSA, Length:2048, Cert Id:-1250580323, Valid from:7/7/16, 1:48 AM, Valid until:12/31/30, 1:00 AM
# 2022-07-08 00:35:47.512 DEBUG 40373 --- [  restartedMain] jdk.event.security                       : X509Certificate: Alg:SHA256withRSA, Serial:4efb7bc62e2b049e, Subject:CN=Legion of the Bouncy Castle Inc., OU=Java Software Code Signing, O=Oracle Corporation, Issuer:CN=JCE Code Signing CA, OU=Java Software Code Signing, O=Oracle Corporation, Key type:DSA, Length:2048, Cert Id:-654023182, Valid from:3/11/17, 2:07 AM, Valid until:3/11/22, 2:07 AM
# 2022-07-08 00:35:47.516 DEBUG 40373 --- [  restartedMain] jdk.event.security                       : X509Certificate: Alg:SHA256withRSA, Serial:4cd3f8568ae76c61bb0fe7160cca76d, Subject:CN=TIMESTAMP-SHA256-2019-10-15, O="DigiCert, Inc.", C=US, Issuer:CN=DigiCert SHA2 Assured ID Timestamping CA, OU=www.digicert.com, O=DigiCert Inc, C=US, Key type:RSA, Length:2048, Cert Id:380147180, Valid from:10/1/19, 2:00 AM, Valid until:10/17/30, 2:00 AM
# 2022-07-08 00:35:47.517 DEBUG 40373 --- [  restartedMain] jdk.event.security                       : X509Certificate: Alg:SHA256withRSA, Serial:aa125d6d6321b7e41e405da3697c215, Subject:CN=DigiCert SHA2 Assured ID Timestamping CA, OU=www.digicert.com, O=DigiCert Inc, C=US, Issuer:CN=DigiCert Assured ID Root CA, OU=www.digicert.com, O=DigiCert Inc, C=US, Key type:RSA, Length:2048, Cert Id:-1680473293, Valid from:1/7/16, 1:00 PM, Valid until:1/7/31, 1:00 PM
# 2022-07-08 00:35:47.895 DEBUG 40373 --- [  restartedMain] i.m.c.u.i.logging.InternalLoggerFactory  : Using SLF4J as the default logging framework
# 2022-07-08 00:35:48.146 DEBUG 40373 --- [  restartedMain] a.campus.demo3.config.WebConfigurer      : Registering CORS filter
# 2022-07-08 00:35:48.198  INFO 40373 --- [  restartedMain] a.campus.demo3.config.WebConfigurer      : Web application configuration, using profiles: dev
# 2022-07-08 00:35:48.200 DEBUG 40373 --- [  restartedMain] a.campus.demo3.config.WebConfigurer      : Initialize H2 console
# 2022-07-08 00:35:48.209  INFO 40373 --- [  restartedMain] a.campus.demo3.config.WebConfigurer      : Web application fully configured
# 2022-07-08 00:35:48.551 DEBUG 40373 --- [  restartedMain] c.ehcache.core.Ehcache-usersByLogin      : Initialize successful.
# 2022-07-08 00:35:48.578 DEBUG 40373 --- [  restartedMain] c.ehcache.core.Ehcache-usersByEmail      : Initialize successful.
# 2022-07-08 00:35:48.586 DEBUG 40373 --- [  restartedMain] c.e.c.E.campus.demo3.domain.User         : Initialize successful.
# 2022-07-08 00:35:48.593 DEBUG 40373 --- [  restartedMain] c.e.c.E.campus.demo3.domain.Authority    : Initialize successful.
# 2022-07-08 00:35:48.599 DEBUG 40373 --- [  restartedMain] c.e.c.E.c.demo3.domain.User.authorities  : Initialize successful.
# 2022-07-08 00:35:48.635 DEBUG 40373 --- [  restartedMain] a.c.demo3.config.AsyncConfiguration      : Creating Async Task Executor
# 2022-07-08 00:35:48.762 DEBUG 40373 --- [  restartedMain] a.c.demo3.config.LiquibaseConfiguration  : Configuring Liquibase
# 2022-07-08 00:35:48.924  WARN 40373 --- [  demo-3-task-1] t.j.c.liquibase.AsyncSpringLiquibase     : Starting Liquibase asynchronously, your database might not be ready at startup!
# 2022-07-08 00:35:50.236 DEBUG 40373 --- [  demo-3-task-1] t.j.c.liquibase.AsyncSpringLiquibase     : Liquibase has updated your database in 1309 ms
# 2022-07-08 00:35:50.514 DEBUG 40373 --- [  restartedMain] a.c.demo3.security.jwt.TokenProvider     : Using a Base64-encoded JWT secret key
# 2022-07-08 00:35:52.002 DEBUG 40373 --- [  restartedMain] a.c.demo3.config.DatabaseConfiguration   : H2 database is available on port 18080
# 2022-07-08 00:35:52.324  WARN 40373 --- [  restartedMain] o.s.s.c.a.web.builders.WebSecurity       : You are asking Spring Security to ignore Ant [pattern='/**', OPTIONS]. This is not recommended -- please use permitAll via HttpSecurity#authorizeHttpRequests instead.
# 2022-07-08 00:35:52.328  WARN 40373 --- [  restartedMain] o.s.s.c.a.web.builders.WebSecurity       : You are asking Spring Security to ignore Ant [pattern='/app/**/*.{js,html}']. This is not recommended -- please use permitAll via HttpSecurity#authorizeHttpRequests instead.
# 2022-07-08 00:35:52.328  WARN 40373 --- [  restartedMain] o.s.s.c.a.web.builders.WebSecurity       : You are asking Spring Security to ignore Ant [pattern='/i18n/**']. This is not recommended -- please use permitAll via HttpSecurity#authorizeHttpRequests instead.
# 2022-07-08 00:35:52.328  WARN 40373 --- [  restartedMain] o.s.s.c.a.web.builders.WebSecurity       : You are asking Spring Security to ignore Ant [pattern='/content/**']. This is not recommended -- please use permitAll via HttpSecurity#authorizeHttpRequests instead.
# 2022-07-08 00:35:52.328  WARN 40373 --- [  restartedMain] o.s.s.c.a.web.builders.WebSecurity       : You are asking Spring Security to ignore Ant [pattern='/h2-console/**']. This is not recommended -- please use permitAll via HttpSecurity#authorizeHttpRequests instead.
# 2022-07-08 00:35:52.328  WARN 40373 --- [  restartedMain] o.s.s.c.a.web.builders.WebSecurity       : You are asking Spring Security to ignore Ant [pattern='/swagger-ui/**']. This is not recommended -- please use permitAll via HttpSecurity#authorizeHttpRequests instead.
# 2022-07-08 00:35:52.329  WARN 40373 --- [  restartedMain] o.s.s.c.a.web.builders.WebSecurity       : You are asking Spring Security to ignore Ant [pattern='/test/**']. This is not recommended -- please use permitAll via HttpSecurity#authorizeHttpRequests instead.
# 2022-07-08 00:35:52.833 DEBUG 40373 --- [  restartedMain] c.a.JHipsterSpringDocGroupsConfiguration : Initializing JHipster OpenApi customizer
# 2022-07-08 00:35:52.842 DEBUG 40373 --- [  restartedMain] c.a.JHipsterSpringDocGroupsConfiguration : Initializing JHipster OpenApi default group
# 2022-07-08 00:35:52.847 DEBUG 40373 --- [  restartedMain] c.a.JHipsterSpringDocGroupsConfiguration : Initializing JHipster OpenApi management group
# 2022-07-08 00:35:53.712  INFO 40373 --- [  restartedMain] org.jboss.threads                        : JBoss Threads version 3.1.0.Final
# 2022-07-08 00:35:53.812  INFO 40373 --- [  restartedMain] academy.campus.demo3.Demo3App            : Started Demo3App in 10.366 seconds (JVM running for 11.043)
# 2022-07-08 00:35:53.817  INFO 40373 --- [  restartedMain] academy.campus.demo3.Demo3App            : 
# ----------------------------------------------------------
# 	Application 'demo3' is running! Access URLs:
# 	Local: 		http://localhost:8080/
# 	External: 	http://127.0.1.1:8080/
# 	Profile(s): 	[dev, api-docs]
# ----------------------------------------------------------
# 
# [...]


### Ouvrir un onglet dans un navigateur avec l'url http://localhost:8080/
#   S'identifer en tant que admin / admin
#   Afficher les différentes vues proposées par l'interface d'administration

### Arrêter l'application
^C
# [INFO] ------------------------------------------------------------------------
# [INFO] BUILD SUCCESS
# [INFO] ------------------------------------------------------------------------
# [INFO] Total time:  08:44 min
# [INFO] Finished at: 2022-07-08T00:43:07+02:00
# [INFO] ------------------------------------------------------------------------

### Pas besoin de consigner ici car jHipster a déjà créé un premier commit

git log
# commit 3b765444e56021662a0b38f65cfe1e0e5574b27d (HEAD -> master)
# Author: Pierre Raoul <p.m.raoul@gmail.com>
# Date:   Fri Jul 8 00:27:08 2022 +0200
# 
#     Initial version of demo3 generated by generator-jhipster@7.8.1


```

## Ajouter un modèle métier

```bash
###########################################################################################################################################################"
###
### Compléter le modèle métier
###
###########################################################################################################################################################"

### jHipster permet de le faire en ligne de commande, élément par élément :

jhipster --help
# INFO! Switching to JHipster installed locally in current project's node repository (node_modules)
# Usage: jhipster [options] [command]
# 
# Options:
#   -V, --version                           output the version number
#   --blueprints <value>                    A comma separated list of one or more generator blueprints to use for the sub generators, e.g. --blueprints kotlin,vuejs
#   --force-insight                         Force insight
#   --no-insight                            Disable insight
#   --force                                 Override every file (default: false)
#   --dry-run                               Print conflicts (default: false)
#   --whitespace                            Whitespace changes will not trigger conflicts (default: false)
#   --bail                                  Fail on first conflict (default: false)
#   --install-path                          Show jhipster install path (default: false)
#   --skip-regenerate                       Don't regenerate identical files (default: false)
#   --skip-yo-resolve                       Ignore .yo-resolve files (default: false)
#   --bundled                               Use JHipster generators bundled with current cli
#   -d, --debug                             enable debugger
#   -h, --help                              display help for command
# 
# Commands:
#   add                                     Add a feature to current project
#   app                                     [Default] Create a new JHipster application based on the selected options
#   aws                                     Deploy the current application to Amazon Web Services
#   azure-app-service                       Deploy the current application to Azure App Service
#   azure-spring-cloud                      Deploy the current application to Azure Spring Cloud
#   ci-cd                                   Create pipeline scripts for popular Continuous Integration/Continuous Deployment tools
#   cloudfoundry                            Generate a `deploy/cloudfoundry` folder with a specific manifest.yml to deploy to Cloud Foundry
#   docker-compose                          Create all required Docker deployment configuration for the selected applications
#   download <jdlFiles...>                  Download jdl file from template repository
#   entity                                  Create a new JHipster entity: JPA entity, Spring server-side components and Angular client-side components
#   entities                                Regenerate entities
#   export-jdl                              Create a JDL file from the existing entities
#   gae                                     Deploy the current application to Google App Engine
#   generate-blueprint                      Generate a blueprint
#   gradle                                  Create Gradle project (alpha)
#   heroku                                  Deploy the current application to Heroku
#   info                                    Display information about your current project and system
#   init                                    Init project (alpha)
#   java                                    Run java generator (alpha)
#   jdl|import-jdl [options] [jdlFiles...]  Create entities from the JDL file/URL/content passed in argument.
#       Use the '--fork' or '--interactive' flag to change the process forking behavior when generating multiple applications.
#   kubernetes|k8s                          Deploy the current application to Kubernetes
#   kubernetes-helm|helm                    Deploy the current application to Kubernetes using Helm package manager
#   kubernetes-knative|knative              Deploy the current application to Kubernetes using knative constructs
#   languages                               Select languages from a list of available languages. The i18n files will be copied to the /webapp/i18n folder
#   maven                                   Create Maven project (alpha)
#   openshift                               Deploy the current application to OpenShift
#   page                                    Create a new page. (Supports vue clients)
#   project-name                            Configure project name (alpha)
#   run [generator]                         Run a module or custom generator
#   spring-boot                             Create a Spring Boot application (alpha)
#   spring-service|service                  Create a new Spring service bean
#   spring-controller                       Create a new Spring controller
#   openapi-client                          Generates java client code from an OpenAPI/Swagger definition
#   upgrade                                 Upgrade the JHipster version, and upgrade the generated application
#   upgrade-config                          Upgrade the JHipster configuration
#   workspaces                              Add workspaces configuration
#   help [command]                          display help for command
# 
#   For more info visit https://www.jhipster.tech

### Mais ici on va utiliser 'jdl', le DSL (domain specific language) fourni par jHipster.
#   On va récupérer un fichier existant pour un modèle métier de type RH
#   disponible depuis https://start.jhipster.tech/jdl-studio/

cat > jhipster-jdl.jdl << '___eof'
entity Region {
    regionName String
}

entity Country {
    countryName String
}

// an ignored comment
/** not an ignored comment */
entity Location {
    streetAddress String,
    postalCode String,
    city String,
    stateProvince String
}

entity Department {
    departmentName String required
}

/**
 * Task entity.
 * @author The JHipster team.
 */
entity Task {
    title String,
    description String
}

/**
 * The Employee entity.
 */
entity Employee {
    /**
    * The firstname attribute.
    */
    firstName String,
    lastName String,
    email String,
    phoneNumber String
    hireDate Instant
    salary Long,
    commissionPct Long
}

entity Job {
    jobTitle String,
    minSalary Long,
    maxSalary Long
}

entity JobHistory {
    startDate Instant,
    endDate Instant,
    language Language
}

enum Language {
    FRENCH, ENGLISH, SPANISH
}

relationship OneToOne {
    Country{region} to Region
}

relationship OneToOne {
    Location{country} to Country
}

relationship OneToOne {
    Department{location} to Location
}

relationship ManyToMany {
    Job{task(title)} to Task{job}
}

// defining multiple OneToMany relationships with comments
relationship OneToMany {
    Employee to Job{employee},
    /**
    * A relationship
    */
    Department to
    /**
    * Another side of the same relationship
    */
    Employee{department}
}

relationship ManyToOne {
    Employee{manager} to Employee
}

// defining multiple oneToOne relationships
relationship OneToOne {
    JobHistory{job} to Job,
    JobHistory{department} to Department,
    JobHistory{employee} to Employee
}

// Set pagination options
paginate JobHistory, Employee with infinite-scroll
paginate Job with pagination

// Use Data Transfer Objects (DTO)
// dto * with mapstruct

// Set service options to all except few
service all with serviceImpl except Employee, Job

// Set an angular suffix
// angularSuffix * with mySuffix
___eof

### Dans le fichier ci-dessus mapstruct n'est pas activé
#   Le modèle métier reste encore suffisemment simple pour ne pas nécessité de mapping


jhipster jdl jhipster-jdl.jdl
# 
#         ██╗ ██╗   ██╗ ████████╗ ███████╗   ██████╗ ████████╗ ████████╗ ███████╗
#         ██║ ██║   ██║ ╚══██╔══╝ ██╔═══██╗ ██╔════╝ ╚══██╔══╝ ██╔═════╝ ██╔═══██╗
#         ██║ ████████║    ██║    ███████╔╝ ╚█████╗     ██║    ██████╗   ███████╔╝
#   ██╗   ██║ ██╔═══██║    ██║    ██╔════╝   ╚═══██╗    ██║    ██╔═══╝   ██╔══██║
#   ╚██████╔╝ ██║   ██║ ████████╗ ██║       ██████╔╝    ██║    ████████╗ ██║  ╚██╗
#    ╚═════╝  ╚═╝   ╚═╝ ╚═══════╝ ╚═╝       ╚═════╝     ╚═╝    ╚═══════╝ ╚═╝   ╚═╝
#                             https://www.jhipster.tech
# Welcome to JHipster v7.8.1
# 
# INFO! Executing import-jdl jhipster-jdl.jdl
# INFO! The JDL is being parsed.
# warn: In the One-to-Many relationship from Employee to Job, only bidirectionality is supported for a One-to-Many association. The other side will be automatically added.
# warn: In the One-to-Many relationship from Department to Employee, only bidirectionality is supported for a One-to-Many association. The other side will be automatically added.
# INFO! Found entities: Region, Country, Location, Department, Task, Employee, Job, JobHistory.
# INFO! The JDL has been successfully parsed
# INFO! Generating 0 applications.
# INFO! Generating 8 entities.
# INFO! Generating entities for application undefined in a new parallel process
# 
# Found the .jhipster/Region.json configuration file, entity can be automatically generated!
# 
# Found the .jhipster/Country.json configuration file, entity can be automatically generated!
# 
# Found the .jhipster/Location.json configuration file, entity can be automatically generated!
# 
# Found the .jhipster/Department.json configuration file, entity can be automatically generated!
# 
# Found the .jhipster/Task.json configuration file, entity can be automatically generated!
# 
# Found the .jhipster/Employee.json configuration file, entity can be automatically generated!
# 
# Found the .jhipster/Job.json configuration file, entity can be automatically generated!
# 
# Found the .jhipster/JobHistory.json configuration file, entity can be automatically generated!
# 
#      info Creating changelog for entities Region,Country,Location,Department,Task,Employee,Job,JobHistory
# WARNING! Error at 'Country' definitions: 'otherEntityRelationshipName' is set with value 'country' at relationship 'region' but no back-reference was found at 'region'
# WARNING! Error at 'Location' definitions: 'otherEntityRelationshipName' is set with value 'location' at relationship 'country' but no back-reference was found at 'country'
# WARNING! Error at 'Department' definitions: 'otherEntityRelationshipName' is set with value 'department' at relationship 'location' but no back-reference was found at 'location'
# WARNING! Error at 'Employee' definitions: 'otherEntityRelationshipName' is set with value 'employee' at relationship 'manager' but no back-reference was found at 'employee'
# WARNING! Error at 'JobHistory' definitions: 'otherEntityRelationshipName' is set with value 'jobHistory' at relationship 'job' but no back-reference was found at 'job'
# WARNING! Error at 'JobHistory' definitions: 'otherEntityRelationshipName' is set with value 'jobHistory' at relationship 'department' but no back-reference was found at 'department'
# WARNING! Error at 'JobHistory' definitions: 'otherEntityRelationshipName' is set with value 'jobHistory' at relationship 'employee' but no back-reference was found at 'employee'
#     force .yo-rc.json
#     force .jhipster/Region.json
#     force .jhipster/Country.json
#     force .jhipster/Location.json
#     force .jhipster/Department.json
#     force .jhipster/Task.json
#     force .jhipster/Employee.json
#     force .jhipster/Job.json
#     force .jhipster/JobHistory.json
#    create src/main/webapp/app/entities/region/region.model.ts
#    create src/main/webapp/app/entities/region/update/region-update.component.html
#  conflict src/main/webapp/i18n/en/global.json
# ? Overwrite src/main/webapp/i18n/en/global.json? overwrite
#     force src/main/webapp/i18n/en/global.json
#    create src/main/webapp/app/entities/country/update/country-update.component.html
#    create src/main/webapp/i18n/fr/country.json
#    create src/main/java/academy/campus/demo3/repository/RegionRepository.java
#    create src/main/webapp/app/entities/region/list/region.component.html
#    create src/main/webapp/app/entities/region/delete/region-delete-dialog.component.html
#    create src/main/webapp/i18n/fr/region.json
#    create src/main/webapp/app/entities/country/delete/country-delete-dialog.component.html
#    create src/main/webapp/app/entities/region/update/region-update.component.ts
#    create src/main/webapp/app/entities/country/country.model.ts
#    create src/main/webapp/app/entities/country/update/country-update.component.ts
#    create src/main/webapp/app/entities/region/detail/region-detail.component.html
#    create src/main/webapp/app/entities/region/delete/region-delete-dialog.component.ts
#    create src/main/webapp/app/entities/country/list/country.component.html
#    create src/main/webapp/app/entities/country/delete/country-delete-dialog.component.ts
#    create src/main/webapp/app/entities/region/detail/region-detail.component.spec.ts
#    create src/main/webapp/app/entities/country/detail/country-detail.component.html
#    create src/main/webapp/app/entities/country/detail/country-detail.component.spec.ts
#    create src/main/webapp/app/entities/region/region.module.ts
#    create src/main/java/academy/campus/demo3/service/RegionService.java
#    create src/main/webapp/app/entities/region/list/region.component.spec.ts
#    create src/main/webapp/app/entities/country/country.module.ts
#    create src/main/webapp/app/entities/country/list/country.component.spec.ts
#    create src/main/webapp/app/entities/region/route/region-routing.module.ts
#    create src/main/webapp/app/entities/region/route/region-routing-resolve.service.spec.ts
#    create src/main/webapp/app/entities/country/route/country-routing.module.ts
#    create src/main/webapp/app/entities/country/route/country-routing-resolve.service.spec.ts
#    create src/main/java/academy/campus/demo3/repository/LocationRepository.java
#    create src/main/java/academy/campus/demo3/service/impl/RegionServiceImpl.java
#    create src/main/webapp/app/entities/region/route/region-routing-resolve.service.ts
#    create src/main/webapp/app/entities/region/service/region.service.spec.ts
#    create src/main/java/academy/campus/demo3/web/rest/CountryResource.java
#    create src/main/webapp/app/entities/country/route/country-routing-resolve.service.ts
#    create src/main/webapp/app/entities/country/service/country.service.spec.ts
#    create src/main/java/academy/campus/demo3/service/LocationService.java
#    create src/test/java/academy/campus/demo3/web/rest/RegionResourceIT.java
#    create src/main/webapp/app/entities/region/list/region.component.ts
#    create src/main/webapp/app/entities/region/update/region-update.component.spec.ts
#    create src/main/java/academy/campus/demo3/repository/CountryRepository.java
#    create src/main/webapp/app/entities/country/list/country.component.ts
#    create src/main/webapp/app/entities/country/update/country-update.component.spec.ts
#    create src/main/java/academy/campus/demo3/service/impl/LocationServiceImpl.java
#    create src/main/java/academy/campus/demo3/web/rest/RegionResource.java
#    create src/main/webapp/app/entities/region/detail/region-detail.component.ts
#    create src/main/webapp/app/entities/region/delete/region-delete-dialog.component.spec.ts
#    create src/main/java/academy/campus/demo3/service/CountryService.java
#    create src/main/webapp/app/entities/country/detail/country-detail.component.ts
#    create src/main/webapp/app/entities/country/delete/country-delete-dialog.component.spec.ts
#    create src/test/java/academy/campus/demo3/web/rest/LocationResourceIT.java
#    create src/test/java/academy/campus/demo3/domain/RegionTest.java
#    create src/main/webapp/app/entities/region/service/region.service.ts
#    create src/main/webapp/i18n/en/region.json
#    create src/main/java/academy/campus/demo3/service/impl/CountryServiceImpl.java
#    create src/main/webapp/app/entities/country/service/country.service.ts
#    create src/main/webapp/i18n/en/country.json
#    create src/test/java/academy/campus/demo3/domain/LocationTest.java
#    create src/test/java/academy/campus/demo3/web/rest/CountryResourceIT.java
#    create src/main/webapp/app/entities/location/location.model.ts
#    create src/test/java/academy/campus/demo3/domain/CountryTest.java
#    create src/main/webapp/app/entities/location/list/location.component.html
#    create src/main/webapp/app/entities/location/detail/location-detail.component.html
#    create src/main/webapp/app/entities/location/detail/location-detail.component.spec.ts
#    create src/main/webapp/app/entities/location/location.module.ts
#    create src/main/webapp/app/entities/location/list/location.component.spec.ts
#    create src/main/webapp/app/entities/location/route/location-routing.module.ts
#    create src/main/webapp/app/entities/location/route/location-routing-resolve.service.spec.ts
#    create src/main/java/academy/campus/demo3/web/rest/LocationResource.java
#    create src/main/webapp/app/entities/location/route/location-routing-resolve.service.ts
#    create src/main/webapp/app/entities/location/list/location.component.ts
#    create src/main/webapp/app/entities/location/detail/location-detail.component.ts
#    create src/main/webapp/app/entities/location/service/location.service.ts
#    create src/main/webapp/i18n/en/location.json
#    create src/main/webapp/app/entities/location/update/location-update.component.html
#    create src/main/webapp/i18n/fr/location.json
#    create src/main/webapp/app/entities/location/delete/location-delete-dialog.component.html
#    create src/main/webapp/app/entities/location/update/location-update.component.ts
#    create src/main/webapp/app/entities/location/delete/location-delete-dialog.component.ts
#    create src/main/webapp/app/entities/location/service/location.service.spec.ts
#    create src/main/java/academy/campus/demo3/web/rest/DepartmentResource.java
#    create src/main/java/academy/campus/demo3/repository/DepartmentRepository.java
#    create src/main/webapp/app/entities/location/update/location-update.component.spec.ts
#    create src/main/java/academy/campus/demo3/service/DepartmentService.java
#    create src/main/webapp/app/entities/location/delete/location-delete-dialog.component.spec.ts
#    create src/main/java/academy/campus/demo3/service/impl/DepartmentServiceImpl.java
#    create src/test/java/academy/campus/demo3/web/rest/DepartmentResourceIT.java
#    create src/test/java/academy/campus/demo3/domain/DepartmentTest.java
#    create src/main/webapp/app/entities/department/department.model.ts
#    create src/main/webapp/app/entities/department/list/department.component.html
#    create src/main/webapp/app/entities/department/detail/department-detail.component.html
#    create src/main/webapp/app/entities/department/department.module.ts
#    create src/main/webapp/app/entities/department/route/department-routing.module.ts
#    create src/main/webapp/app/entities/department/detail/department-detail.component.spec.ts
#    create src/main/webapp/app/entities/department/list/department.component.spec.ts
#    create src/main/webapp/app/entities/department/route/department-routing-resolve.service.ts
#    create src/main/webapp/app/entities/department/route/department-routing-resolve.service.spec.ts
#    create src/main/webapp/app/entities/department/list/department.component.ts
#    create src/main/webapp/app/entities/department/detail/department-detail.component.ts
#    create src/main/webapp/app/entities/department/service/department.service.ts
#    create src/main/webapp/app/entities/department/update/department-update.component.html
#    create src/main/webapp/i18n/en/department.json
#    create src/main/webapp/app/entities/department/delete/department-delete-dialog.component.html
#    create src/main/webapp/i18n/fr/department.json
#    create src/main/webapp/app/entities/department/update/department-update.component.ts
#    create src/main/webapp/app/entities/department/delete/department-delete-dialog.component.ts
#    create src/main/java/academy/campus/demo3/web/rest/TaskResource.java
#    create src/main/java/academy/campus/demo3/repository/TaskRepository.java
#    create src/main/webapp/app/entities/department/service/department.service.spec.ts
#    create src/main/java/academy/campus/demo3/service/TaskService.java
#    create src/main/webapp/app/entities/department/update/department-update.component.spec.ts
#    create src/main/java/academy/campus/demo3/service/impl/TaskServiceImpl.java
#    create src/main/webapp/app/entities/department/delete/department-delete-dialog.component.spec.ts
#    create src/test/java/academy/campus/demo3/web/rest/TaskResourceIT.java
#    create src/test/java/academy/campus/demo3/domain/TaskTest.java
#    create src/main/webapp/app/entities/task/task.model.ts
#    create src/main/webapp/app/entities/task/task.module.ts
#    create src/main/webapp/app/entities/task/list/task.component.html
#    create src/main/webapp/app/entities/task/detail/task-detail.component.html
#    create src/main/webapp/app/entities/task/route/task-routing.module.ts
#    create src/main/webapp/app/entities/task/detail/task-detail.component.spec.ts
#    create src/main/webapp/app/entities/task/route/task-routing-resolve.service.ts
#    create src/main/webapp/app/entities/task/list/task.component.spec.ts
#    create src/main/webapp/app/entities/task/list/task.component.ts
#    create src/main/webapp/app/entities/task/route/task-routing-resolve.service.spec.ts
#    create src/main/webapp/app/entities/task/detail/task-detail.component.ts
#    create src/main/webapp/app/entities/task/service/task.service.ts
#    create src/main/webapp/app/entities/task/update/task-update.component.html
#    create src/main/webapp/i18n/en/task.json
#    create src/main/webapp/app/entities/task/delete/task-delete-dialog.component.html
#    create src/main/webapp/i18n/fr/task.json
#    create src/main/webapp/app/entities/task/update/task-update.component.ts
#    create src/main/webapp/app/entities/task/delete/task-delete-dialog.component.ts
#    create src/main/java/academy/campus/demo3/web/rest/EmployeeResource.java
#    create src/main/webapp/app/entities/task/service/task.service.spec.ts
#    create src/main/java/academy/campus/demo3/repository/EmployeeRepository.java
#    create src/main/webapp/app/entities/task/update/task-update.component.spec.ts
#    create src/test/java/academy/campus/demo3/web/rest/EmployeeResourceIT.java
#    create src/test/java/academy/campus/demo3/domain/EmployeeTest.java
#    create src/main/webapp/app/entities/task/delete/task-delete-dialog.component.spec.ts
#    create src/main/webapp/app/entities/employee/employee.model.ts
#    create src/main/webapp/app/entities/employee/list/employee.component.html
#    create src/main/webapp/app/entities/employee/detail/employee-detail.component.html
#    create src/main/webapp/app/entities/employee/employee.module.ts
#    create src/main/webapp/app/entities/employee/route/employee-routing.module.ts
#    create src/main/webapp/app/entities/employee/detail/employee-detail.component.spec.ts
#    create src/main/webapp/app/entities/employee/list/employee.component.spec.ts
#    create src/main/webapp/app/entities/employee/route/employee-routing-resolve.service.ts
#    create src/main/webapp/app/entities/employee/route/employee-routing-resolve.service.spec.ts
#    create src/main/webapp/app/entities/employee/list/employee.component.ts
#    create src/main/webapp/app/entities/employee/detail/employee-detail.component.ts
#    create src/main/webapp/app/entities/employee/service/employee.service.ts
#    create src/main/webapp/i18n/en/employee.json
#    create src/main/webapp/app/entities/employee/update/employee-update.component.html
#    create src/main/webapp/i18n/fr/employee.json
#    create src/main/webapp/app/entities/employee/delete/employee-delete-dialog.component.html
#    create src/main/webapp/app/entities/employee/update/employee-update.component.ts
#    create src/main/webapp/app/entities/employee/delete/employee-delete-dialog.component.ts
#    create src/main/java/academy/campus/demo3/web/rest/JobResource.java
#    create src/main/webapp/app/entities/employee/service/employee.service.spec.ts
#    create src/main/java/academy/campus/demo3/repository/JobRepository.java
#    create src/main/webapp/app/entities/employee/update/employee-update.component.spec.ts
#    create src/main/java/academy/campus/demo3/repository/JobRepositoryWithBagRelationships.java
#    create src/main/java/academy/campus/demo3/repository/JobRepositoryWithBagRelationshipsImpl.java
#    create src/main/webapp/app/entities/employee/delete/employee-delete-dialog.component.spec.ts
#    create src/test/java/academy/campus/demo3/web/rest/JobResourceIT.java
#    create src/test/java/academy/campus/demo3/domain/JobTest.java
#    create src/main/webapp/app/entities/job/job.model.ts
#    create src/main/webapp/app/entities/job/list/job.component.html
#    create src/main/webapp/app/entities/job/detail/job-detail.component.html
#    create src/main/webapp/app/entities/job/job.module.ts
#    create src/main/webapp/app/entities/job/route/job-routing.module.ts
#    create src/main/webapp/app/entities/job/detail/job-detail.component.spec.ts
#    create src/main/webapp/app/entities/job/route/job-routing-resolve.service.ts
#    create src/main/webapp/app/entities/job/list/job.component.spec.ts
#    create src/main/webapp/app/entities/job/list/job.component.ts
#    create src/main/webapp/app/entities/job/route/job-routing-resolve.service.spec.ts
#    create src/main/webapp/app/entities/job/detail/job-detail.component.ts
#    create src/main/webapp/app/entities/job/service/job.service.ts
#    create src/main/webapp/app/entities/job/update/job-update.component.html
#    create src/main/webapp/i18n/en/job.json
#    create src/main/webapp/app/entities/job/delete/job-delete-dialog.component.html
#    create src/main/webapp/i18n/fr/job.json
#    create src/main/webapp/app/entities/job/update/job-update.component.ts
#    create src/main/webapp/app/entities/job/delete/job-delete-dialog.component.ts
#    create src/main/webapp/app/entities/job/service/job.service.spec.ts
#    create src/main/java/academy/campus/demo3/web/rest/JobHistoryResource.java
#    create src/main/webapp/app/entities/job/update/job-update.component.spec.ts
#    create src/main/java/academy/campus/demo3/repository/JobHistoryRepository.java
#    create src/main/webapp/app/entities/job/delete/job-delete-dialog.component.spec.ts
#    create src/main/java/academy/campus/demo3/service/JobHistoryService.java
#    create src/main/java/academy/campus/demo3/service/impl/JobHistoryServiceImpl.java
#    create src/test/java/academy/campus/demo3/web/rest/JobHistoryResourceIT.java
#    create src/test/java/academy/campus/demo3/domain/JobHistoryTest.java
#    create src/main/webapp/app/entities/enumerations/language.model.ts
#    create src/main/webapp/app/entities/job-history/job-history.model.ts
#    create src/main/webapp/app/entities/job-history/list/job-history.component.html
#    create src/main/webapp/app/entities/job-history/detail/job-history-detail.component.html
#    create src/main/webapp/app/entities/job-history/job-history.module.ts
#    create src/main/webapp/app/entities/job-history/route/job-history-routing.module.ts
#    create src/main/webapp/app/entities/job-history/detail/job-history-detail.component.spec.ts
#    create src/main/webapp/app/entities/job-history/list/job-history.component.spec.ts
#    create src/main/webapp/app/entities/job-history/route/job-history-routing-resolve.service.ts
#    create src/main/webapp/app/entities/job-history/route/job-history-routing-resolve.service.spec.ts
#    create src/main/webapp/app/entities/job-history/list/job-history.component.ts
#    create src/main/java/academy/campus/demo3/domain/enumeration/Language.java
#    create src/main/webapp/app/entities/job-history/detail/job-history-detail.component.ts
#    create src/main/webapp/app/entities/job-history/service/job-history.service.ts
#    create src/main/webapp/i18n/en/language.json
#    create src/main/webapp/app/entities/job-history/update/job-history-update.component.html
#    create src/main/webapp/i18n/fr/language.json
#    create src/main/webapp/app/entities/job-history/delete/job-history-delete-dialog.component.html
#    create src/main/webapp/i18n/en/jobHistory.json
#    create src/main/webapp/app/entities/job-history/update/job-history-update.component.ts
#    create src/main/webapp/i18n/fr/jobHistory.json
#    create src/main/resources/config/liquibase/changelog/20220707230949_added_entity_Region.xml
#    create src/main/webapp/app/entities/job-history/delete/job-history-delete-dialog.component.ts
#    create src/main/resources/config/liquibase/fake-data/region.csv
#    create src/main/webapp/app/entities/job-history/service/job-history.service.spec.ts
#    create src/main/resources/config/liquibase/changelog/20220707230950_added_entity_Country.xml
#    create src/main/resources/config/liquibase/changelog/20220707230950_added_entity_constraints_Country.xml
#    create src/main/webapp/app/entities/job-history/update/job-history-update.component.spec.ts
#    create src/main/resources/config/liquibase/fake-data/country.csv
#    create src/main/webapp/app/entities/job-history/delete/job-history-delete-dialog.component.spec.ts
#    create src/main/resources/config/liquibase/changelog/20220707230951_added_entity_Location.xml
#    create src/main/resources/config/liquibase/changelog/20220707230951_added_entity_constraints_Location.xml
#    create src/main/resources/config/liquibase/fake-data/location.csv
#    create src/main/resources/config/liquibase/changelog/20220707230952_added_entity_Department.xml
#    create src/main/resources/config/liquibase/changelog/20220707230952_added_entity_constraints_Department.xml
#    create src/main/resources/config/liquibase/fake-data/department.csv
#    create src/main/resources/config/liquibase/changelog/20220707230953_added_entity_Task.xml
#    create src/main/resources/config/liquibase/fake-data/task.csv
#    create src/main/resources/config/liquibase/changelog/20220707230954_added_entity_Employee.xml
#    create src/main/resources/config/liquibase/changelog/20220707230954_added_entity_constraints_Employee.xml
#    create src/main/resources/config/liquibase/fake-data/employee.csv
#    create src/main/resources/config/liquibase/changelog/20220707230955_added_entity_Job.xml
#    create src/main/resources/config/liquibase/changelog/20220707230955_added_entity_constraints_Job.xml
#    create src/main/resources/config/liquibase/fake-data/job.csv
#    create src/main/resources/config/liquibase/changelog/20220707230956_added_entity_JobHistory.xml
#    create src/main/resources/config/liquibase/changelog/20220707230956_added_entity_constraints_JobHistory.xml
#    create src/main/resources/config/liquibase/fake-data/job_history.csv
#    create src/main/java/academy/campus/demo3/domain/Region.java
#    create src/main/java/academy/campus/demo3/domain/Country.java
#    create src/main/java/academy/campus/demo3/domain/Location.java
#    create src/main/java/academy/campus/demo3/domain/Department.java
#    create src/main/java/academy/campus/demo3/domain/Task.java
#    create src/main/java/academy/campus/demo3/domain/Employee.java
#    create src/main/java/academy/campus/demo3/domain/Job.java
#    create src/main/java/academy/campus/demo3/domain/JobHistory.java
#  conflict src/main/webapp/i18n/fr/global.json
# ? Overwrite src/main/webapp/i18n/fr/global.json? overwrite
#     force src/main/webapp/i18n/fr/global.json
#  conflict src/main/webapp/app/entities/entity-routing.module.ts
# ? Overwrite src/main/webapp/app/entities/entity-routing.module.ts? overwrite
#     force src/main/webapp/app/entities/entity-routing.module.ts
#  conflict src/main/java/academy/campus/demo3/config/CacheConfiguration.java
# ? Overwrite src/main/java/academy/campus/demo3/config/CacheConfiguration.java? overwrite
#     force src/main/java/academy/campus/demo3/config/CacheConfiguration.java
#  conflict src/main/resources/config/liquibase/master.xml
# ? Overwrite src/main/resources/config/liquibase/master.xml? overwrite
#     force src/main/resources/config/liquibase/master.xml
#  conflict src/main/webapp/app/layouts/navbar/navbar.component.html
# ? Overwrite src/main/webapp/app/layouts/navbar/navbar.component.html? overwrite
#     force src/main/webapp/app/layouts/navbar/navbar.component.html
#     force .yo-rc.json
#     force .jhipster/Region.json
#     force .jhipster/Country.json
#     force .jhipster/Location.json
#     force .jhipster/Department.json
#     force .jhipster/Task.json
#     force .jhipster/Employee.json
#     force .jhipster/Job.json
#     force .jhipster/JobHistory.json
# 
# No change to package.json was detected. No package manager install will be executed.
# Entity Region generated successfully.
# Entity Country generated successfully.
# Entity Location generated successfully.
# Entity Department generated successfully.
# Entity Task generated successfully.
# Entity Employee generated successfully.
# Entity Job generated successfully.
# Entity JobHistory generated successfully.
# 
# Running `webapp:build` to update client app
# 
# 
# > demo-3@0.0.1-SNAPSHOT webapp:build /home/pierre/workspace/tutorial-jhipster-docker-demo3
# > npm run clean-www && npm run webapp:build:dev
# 
# 
# > demo-3@0.0.1-SNAPSHOT clean-www /home/pierre/workspace/tutorial-jhipster-docker-demo3
# > rimraf target/classes/static/app/{src,target/}
# 
# 
# > demo-3@0.0.1-SNAPSHOT webapp:build:dev /home/pierre/workspace/tutorial-jhipster-docker-demo3
# > ng build --configuration development
# 
# ✔ Browser application bundle generation complete.
# ✔ Copying assets complete.
# ✔ Index html generation complete.
# 
# Initial Chunk Files                                                    | Names                                  |  Raw Size
# styles.css                                                             | styles                                 | 205.42 kB | 
# polyfills.js                                                           | polyfills                              | 134.22 kB | 
# runtime.js                                                             | runtime                                |  12.45 kB | 
# main.js                                                                | main                                   | 776 bytes | 
# 
#                                                                        | Initial Total                          | 352.85 kB
# 
# Lazy Chunk Files                                                       | Names                                  |  Raw Size
# src_main_webapp_bootstrap_ts.js                                        | bootstrap                              |   4.85 MB | 
# src_main_webapp_app_account_account_module_ts.js                       | account-account-module                 | 179.59 kB | 
# src_main_webapp_app_admin_metrics_metrics_module_ts.js                 | metrics-metrics-module                 | 173.04 kB | 
# src_main_webapp_app_entities_job-history_job-history_module_ts.js      | job-history-job-history-module         | 116.56 kB | 
# src_main_webapp_app_admin_user-management_user-management_module_ts.js | user-management-user-management-module | 115.95 kB | 
# src_main_webapp_app_entities_employee_employee_module_ts.js            | employee-employee-module               | 110.06 kB | 
# src_main_webapp_app_entities_job_job_module_ts.js                      | job-job-module                         |  92.13 kB | 
# src_main_webapp_app_entities_location_location_module_ts.js            | location-location-module               |  79.97 kB | 
# src_main_webapp_app_entities_department_department_module_ts.js        | department-department-module           |  73.52 kB | 
# src_main_webapp_app_entities_country_country_module_ts.js              | country-country-module                 |  70.42 kB | 
# src_main_webapp_app_entities_task_task_module_ts.js                    | task-task-module                       |  63.37 kB | 
# src_main_webapp_app_entities_region_region_module_ts.js                | region-region-module                   |  60.98 kB | 
# common.js                                                              | common                                 |  41.76 kB | 
# src_main_webapp_app_admin_health_health_module_ts.js                   | health-health-module                   |  30.11 kB | 
# src_main_webapp_app_admin_configuration_configuration_module_ts.js     | configuration-configuration-module     |  25.87 kB | 
# src_main_webapp_app_admin_logs_logs_module_ts.js                       | logs-logs-module                       |  24.08 kB | 
# src_main_webapp_app_login_login_module_ts.js                           | login-login-module                     |  17.75 kB | 
# src_main_webapp_app_entities_entity-routing_module_ts.js               | entities-entity-routing-module         |   5.31 kB | 
# src_main_webapp_app_admin_docs_docs_module_ts.js                       | docs-docs-module                       |   4.71 kB | 
# src_main_webapp_app_admin_admin-routing_module_ts.js                   | admin-admin-routing-module             |   4.11 kB | 
# 
# Build at: 2022-07-07T23:11:17.837Z - Hash: 214dee0f44fe33cf - Time: 36177ms
# INFO! Generator entities succeed
# Congratulations, JHipster execution is complete!
# Sponsored with ❤️  by @oktadev.


./mvnw
# [...]

### Ouvrir un onglet avec l'url http://localhost:8080/
#   Et se connecter en tant que admin/admin
#   Afficher la liste des Entitées

git add .

git commit -m "Ajouter un domaine métier"
# [...]

```

## Enrichir l'IHM à partir du modèle métier ci-dessus

À vous de jouer...

En fait vous pouvez utiliser tout autre modèle métier suivant votre inclinaison.
On trouve en ligne un certain nombre de fichers jdl prêts à l'emploi, par ex. :
- https://raw.githubusercontent.com/jhipster/jdl-samples/main/simple-online-shop.jh
- https://raw.githubusercontent.com/ksilz/bpf-tutorial-jhipster-docker/master/simple-shop-app.jdl
L'analyse de ceux-ci permettent de se faire une idée des capacités de modélisation de jHispter
